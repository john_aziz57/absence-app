package settings;

import com.john_aziz57.absence.R;

import data.SettingsManager;
import absence.Controller;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarActivity;
import android.text.method.ScrollingMovementMethod;
import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RadioButton;
import android.widget.TextView;

public class Settings extends ActionBarActivity {
	private Controller ctrl;
	private String oLang = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings);
		ctrl = (Controller) getApplicationContext();

		// mirror the settings retrieved from the ctrl's settings manager to the
		// interface
		RadioButton rb=null;
		if (ctrl.getLanguage().equals(SettingsManager.ENGLISH)) {
			rb = (RadioButton)findViewById(R.id.s_rb_en);
		}else if (ctrl.getLanguage().equals(SettingsManager.ARABIC)){
			rb = (RadioButton)findViewById(R.id.s_rb_ar);
		}
		rb.setChecked(true);
		
		TextView tv = (TextView) findViewById(R.id.s_tv_about);
		tv.setMovementMethod(new ScrollingMovementMethod());
	}

	@SuppressLint("InlinedApi")
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.settings, menu);

		MenuItem locationItem = menu
				.add(0, R.id.s_ab_done, 0, R.string.done);
		locationItem.setIcon(R.drawable.ic_action_done);

		MenuItemCompat.setShowAsAction(locationItem,
				MenuItem.SHOW_AS_ACTION_IF_ROOM);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.s_ab_done) {
			// pass the settings to the Controller
			ctrl.changeLangSettings(oLang);
			// write the settings
			ctrl.saveSettings();

			// finish the settings
			finish();

			// startActivity(getIntent());
		}
		return super.onOptionsItemSelected(item);
	}

	public void onRadioButtonClicked(View view) {
		//boolean checked = ((RadioButton) view).isChecked();
		switch (view.getId()) {
		case R.id.s_rb_en:
			oLang = SettingsManager.ENGLISH;
			break;
		case R.id.s_rb_ar:
			oLang = SettingsManager.ARABIC;
			break;
		}

	}

}
