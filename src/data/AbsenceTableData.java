package data;

public class AbsenceTableData {
	private final static String[] months =
		{"Jan",		"Feb",		"Mar",		"Apr",
		"May",		"June",		"July",		"Aug",
		"Sept",		"Oct",		"Nov",		"Dec"};
	private String code;
	private String name;
	private String date;
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDate() {
		return date;
	}
	public String getFormatedDate(){
		String year = date.substring(0, 4);
		String month = months[Integer.parseInt(date.substring(4, 6))-1];
		String day = date.substring(6, 8);
		int hour = Integer.parseInt(date.substring(8, 10));
		String amPm ="am";
		if(hour>12){
			hour-=12;
			amPm="pm";
		}
		String min = date.substring(10, 12);
		String sep="-";
		return day+sep+month+sep+year+" "+hour+":"+min+" "+amPm;
	}
	public void setDate(String date) {
		this.date = date;
	}
	
}
