package data;

public class Constants {
	public static final String BT_SHEET = "BT_SHEET";
	
	/**
	 * Handlers
	 */
	public static final int RECEIVE_SHEET_DONE = 0;
	public static final int RECEIVE_FILE = 1; 
	public static final int FILE_SELECT_CODE = 2;
	
	// send Fragment Handlers
	public static final int SET_STATE = 3;
	// send Fragment State
	public static final int SEND_STATE_CONNECTING = 0;
	public static final int SEND_STATE_SENDING = 1;
	
	// RECEIVE FRAGMENT HANDLER
	public static final int SET_RECEIVE_STATE = 4;
	// recieve Fragment State
	public static final int RECEIVE_STATE_ACCPETING = 0;
	public static final int RECEIVE_STATE_RECEIVING = 1;
	
	
	//Date Formate
	public static final String DATE_FORMAT = "DD-MM-YYYY";
}
