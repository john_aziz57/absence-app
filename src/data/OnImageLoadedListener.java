package data;

import android.graphics.Bitmap;

public interface OnImageLoadedListener {
	public void onImageLoaded(Bitmap image);
}
