package data;
/**
 * Singleton Class
 * The settings file will be like this 
 * 
 * LANGUAGE EN
 * VOLUME 5
 */
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Scanner;

import absence.Controller;
import android.content.Context;
// Singleton  
public class SettingsManager {
	
	private static SettingsManager sMan;
	public static final SettingsManager getSettingsManager(Controller ctrl){
		if(sMan == null)
			sMan =new SettingsManager(ctrl);
		return sMan;
	}
	
	public static final String ENGLISH ="en";
	public static final String ARABIC ="ar";
	
	
	private static final String FILE_NAME ="settings";

	private static final String DEFAULT_LANGUAGE =ENGLISH;
	
	/*
	 * The indexes for the parameters written in the file
	 */
	private static final String LANGUAGE ="LANGUAGE";
	
	/*
	 * The parameters retreived from the file
	 */
	private String oLanguage =DEFAULT_LANGUAGE;//language option
	
	/**
	 * to indicate that it has already read the settings or not, should be set to 
	 * true once reading the settings was done.
	 */
	private boolean haveReadSettings=false; 
	private Controller ctrl;
	
	private SettingsManager(Controller input1){
		ctrl=input1;
	}
	
	public void readSettings(){
		FileInputStream inputStream=null;
		try {
			  inputStream = ctrl.openFileInput(FILE_NAME);
			  Scanner scan = new Scanner(inputStream);
			  while(scan.hasNext()){
				  String option = scan.next();
				  option.trim();
				  // see the options 
				  // TODO stopped here doesn't match the two strings
				  if(option.equals(LANGUAGE)){
					  readLanguage(scan.next());
				  }
			  }
			  inputStream.close();
			} catch (Exception e) {// file not found something is wrong in the file
			  writeSettings();
			}
		
		haveReadSettings=true;
	}
	
	public void writeSettings(){
		FileOutputStream outputStream;
		try {
			  outputStream = ctrl.openFileOutput(FILE_NAME, Context.MODE_PRIVATE);
			  // writing the language
			  outputStream.write(new String(LANGUAGE+" "+oLanguage).getBytes());
			  
			  // close the stream
			  outputStream.close();
			} catch (Exception e) {
			  e.printStackTrace();
			}
	}
	
	private void readLanguage(String lang){
		oLanguage=lang;
	}
	
	public String getLanguage(){
		if(!haveReadSettings){
			readSettings();
		}
		return oLanguage;
	}
	
	public void setLanguage(String lang){
		oLanguage=lang;// TO put switch case in case we wanted to check if the
		// values are right
		
		// write the changes // TODO to commet later cause writing will be done once
		// at the end of this activity
		//writeSettings();
	}
}
