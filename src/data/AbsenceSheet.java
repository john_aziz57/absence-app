package data;

import java.io.Serializable;
import java.util.ArrayList;

public class AbsenceSheet implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4627550175405370031L;
	private String name ="";
	private ArrayList<AbsenceContact> cntctList;
	
	public AbsenceSheet(){
		cntctList = new ArrayList<AbsenceContact>();
	}
	
	public void addContact(AbsenceContact cntct){
		cntctList.add(cntct);
	}
	
	public String getSheetName(){
		return name;
	}
	
	public void setName(String newName){
		name= newName;
	}
	
	public AbsenceContact getContact(int index){
		return cntctList.get(index);
	}
	
	public int getSheetSize(){
		return cntctList.size();
	}
}
