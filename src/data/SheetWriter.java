package data;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class SheetWriter {
	private AbsenceSheet mSheet;
	private File mFile;
	private SheetFileWriter writer;
//	private SheetStreamWriter writer;
	// replicated in both writer and reader TODO remove duplication

	
	/***
	 * 
	 * @param file The path of  the directory that it will write to
	 * @param sheet the data of the absence table
	 */
	public SheetWriter(File file, AbsenceSheet sheet){
		this.mFile = new File(file.getAbsolutePath()+"/"+sheet.getSheetName());
		this.mSheet= sheet;
		try {
			mFile.createNewFile();
			if(mFile.canWrite()){
				writer = new SheetFileWriter(mFile);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public SheetWriter(OutputStream out,AbsenceSheet sheet){
		this.mSheet= sheet;
		try {
			writer = new SheetFileWriter(out);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void writeSheet(){
		try {
			
			writeStartOfSheet(writer);
			for(int i =0;i<mSheet.getSheetSize();i++){
				writeContact(writer, mSheet.getContact(i));
			}
			writeEndOfSheet(writer);
			writer.flush();
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	/**
	 * To write the start of the sheet
	 * @param writer
	 * @throws IOException
	 */
	private void writeStartOfSheet(SheetFileWriter writer) throws IOException{
		writer.writeNewLine(StreamConst.SH_ST);
		writer.writeNewLine(StreamConst.SH_NAME+mSheet.getSheetName());
	}
	
	/**
	 * To write the end of the Sheet and end the writer
	 * @param writer
	 * @throws IOException
	 */
	private void writeEndOfSheet(SheetFileWriter writer) throws IOException{
		writer.writeNewLine(StreamConst.SH_END);
	}
	
	
	private void writeContact(SheetFileWriter writer,AbsenceContact contact) throws IOException{
		writer.writeNewLine(StreamConst.CN_ST);
		writer.writeNewLine(StreamConst.CN_NAME+contact.getName());
		writer.writeNewLine(StreamConst.CN_TEL+contact.getNumber());
		writer.writeNewLine(StreamConst.CN_TEL_FA+contact.getFatherNumber());
		writer.writeNewLine(StreamConst.CN_TEL_MO+contact.getMotherNumber());
		writer.writeNewLine(StreamConst.CN_TEL_HM+contact.getHomeNumber());
		writer.writeNewLine(StreamConst.CN_BD+contact.birthdayToString());
		writer.writeNewLine(StreamConst.CN_ADD+contact.getAddress());
		if(contact.hasImage())
			writer.writeImage(contact.getImagePath());
		else{
			
		}
		writer.writeNewLine(StreamConst.CN_END);
		
	}
	
	
	
	/**
	 * A class to write with adding New Line "\n" at the end of each
	 * write
	 * @author John
	 *
	 */
	private class SheetFileWriter extends OutputStreamWriter {
		public SheetFileWriter(File file) throws IOException {
			super(new FileOutputStream(file));
			
		}
		
		public SheetFileWriter(OutputStream out) throws IOException {
//			OutputStreamWriter writer = new OutputStreamWriter(out);
			super(out);
		}
		
		public void writeNewLine (String str) throws IOException {
			super.write(str+StreamConst.NEW_LINE);
		}
		
		public void writeImage(String path){
			try {
				writeNewLine(StreamConst.IMAGE_START);
				
				Bitmap image = BitmapFactory.decodeFile(path);
				int width= image.getWidth();
				int height = image.getHeight();
				
				writeNewLine(StreamConst.IMAGE_WIDTH+width);
				writeNewLine(StreamConst.IMAGE_HEIGHT+height);
				writeNewLine(StreamConst.IMAGE_PIXEL_DATA);
				int pixelData [] = new int [image.getWidth()*image.getHeight()];
				image.getPixels(pixelData, 0, width, 0, 0, width, height);
				writeArray(pixelData);
				writeNewLine(StreamConst.IMAGE_END);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		public void writeArray(int[] pixelData) throws IOException {
			for(int pixel : pixelData){
				writeNewLine(pixel+"");
			}
			
		}


		
	}
}
