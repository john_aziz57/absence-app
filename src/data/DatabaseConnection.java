package data;

import data.DatabaseConstants.ContactsTable;
import data.DatabaseConstants.MainAbsenceTable;
import data.DatabaseConstants.SheetMeta;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class DatabaseConnection  extends SQLiteOpenHelper {
	//public static final String DATABASE_NAME = "ABSENCE.DB";
	public static final int DATABASE_VERSION =  2;
	public DatabaseConnection(Context context,String dbName){
		super(context,dbName,null,DATABASE_VERSION);
		
	}
	
	@Override
	public void onCreate(SQLiteDatabase arg0) {
		arg0.execSQL(MainAbsenceTable.DROP_TABLE);
		arg0.execSQL(MainAbsenceTable.CREATE_STRING);
		arg0.execSQL(ContactsTable.DROP_TABLE);
		arg0.execSQL(ContactsTable.CREATE_STRING);
		
		arg0.execSQL(SheetMeta.DROP_TABLE);
		arg0.execSQL(SheetMeta.CREATE_STRING);
		arg0.execSQL(SheetMeta.Q_INITIAL_INSERT);
		
	}
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		if(oldVersion == 1){
			String upgradeQuery = "ALTER TABLE "+ ContactsTable.TABLE_NAME +" ADD COLUMN "+ContactsTable.COLUMN_IMAGE_PATH+" TEXT";
		    if (oldVersion == 1 && newVersion == 2)
		         db.execSQL(upgradeQuery);
		}
	}
	
	@Override
	public void onOpen(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		super.onOpen(db);
		db.execSQL("PRAGMA foreign_keys = ON;");
	}
}
