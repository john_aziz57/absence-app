package data;

public class StreamConst {
	public static final String SH_ST="SheetStart",
			  SH_NAME = "SheetName: ",
			  CN_ST = "ContactStart",
			  CN_NAME = "Name: ",
			  CN_TEL = "Telephone: ",
			  CN_TEL_FA = "TelephoneFather: ",
			  CN_TEL_MO = "TelephoneMother: ",
			  CN_TEL_HM = "TelephoneHome: ",
			  CN_BD = "Birthday: ",
			  CN_ADD = "Address: ",
			  CN_END = "ContactEnd",
			  SH_END = "SheetEnd",
			  IMAGE_START ="ImageStart",
			  IMAGE_WIDTH="Width: ",
			  IMAGE_HEIGHT="Height: ",
			  IMAGE_PIXEL_DATA="PixelData: ",
			  IMAGE_END ="ImageEnd",
			  NEW_LINE="\n";
}
