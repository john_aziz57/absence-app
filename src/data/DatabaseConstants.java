package data;

import android.provider.BaseColumns;

public final class DatabaseConstants {

	public static final String TEXT_TYPE = " TEXT ";
	public static final String INTEGER_TYPE = " INTEGER ";
	public static final String COMMA_SEP = ",";
	public static final String PRIMARY_KEY = " PRIMARY KEY ";

	// public static final String TABLE_PREFIX = "ABSNC_LIST_";// the name of
	// the list
	public static final String DB_PREFIX = "DB";

	// THE MAIN ABSENCE TABLE IS ONLY ONE FOR EVERY SHEET.
	public static final String ABSENCE_TABLE_PREFIX = "AT";// the name
															// for each
															// absence
															// table

	public static final int BOOLEAN_TRUE = 1;
	public static final int BOOLEAN_FALSE = 0;

	/**
	 * For each sheet it has its own database This table contains the names of
	 * the databases and their files names Why ? to make the size of the
	 * databases files small not one having all
	 * 
	 * @author John
	 * 
	 */
	public static abstract class Alpha implements BaseColumns {
		public static final String TABLE_NAME = "ALPHA";
		public static final String COLUMN_SHEET_NAME = "NAME";
		public static final String COLUMN_DB_NAME = "DB_NAME";

		public static final String CREATE_STRING = "CREATE TABLE "
				+ Alpha.TABLE_NAME + " (" + Alpha.COLUMN_DB_NAME + " "
				+ TEXT_TYPE + PRIMARY_KEY + COMMA_SEP + Alpha.COLUMN_SHEET_NAME
				+ " " + TEXT_TYPE + " )";

		public static final String DROP_TABLE = " DROP TABLE IF EXISTS "
				+ Alpha.TABLE_NAME;

		public static final String SQL_COUNT = "SELECT COUNT(*) FROM "
				+ Alpha.TABLE_NAME;

	}

	/**
	 * This table is only to give a new id -depending on their count- to the new
	 * database created for new absence sheet this number won't be decreased
	 * when the database is deleted its target is only to increment, till it
	 * reach certain limit // TODO HANDLE THE LIMIT LATER
	 * 
	 * @author John
	 *
	 */
	public static abstract class AlphaMeta implements BaseColumns {
		public static final String TABLE_NAME = "ALPHA_META";
		public static final String COLUMN_NUM_OF_DB = "NUM";
		private static final byte INITIAL_VALUE = -1;

		public static final String CREATE_QUERY = "CREATE TABLE " + TABLE_NAME
				+ " (" + COLUMN_NUM_OF_DB + " " + INTEGER_TYPE + TEXT_TYPE
				+ " )";

		public static final String DROP_TABLE = " DROP TABLE IF EXISTS "
				+ TABLE_NAME;

		public static final String Q_GET_DB_NUM = "SELECT " + COLUMN_NUM_OF_DB
				+ " FROM " + TABLE_NAME;
		/**
		 * increase the number of DBs by one
		 */
		public static final String Q_UPDATE_NUM_OF_DB = "UPDATE " + TABLE_NAME
				+ " SET " + COLUMN_NUM_OF_DB + " = " + COLUMN_NUM_OF_DB + "+1 ";

		public static final String Q_INITIAL_INSERT = "INSERT INTO "
				+ TABLE_NAME + " VALUES(" + INITIAL_VALUE + ")";

	}

	/**
	 * Contains the number of absence tables taken so as to give name to the new
	 * absence table the doesn't repeat itself, only one table created during
	 * the life time of the sheet
	 * 
	 * @author John
	 *
	 */
	public static abstract class SheetMeta implements BaseColumns {
		public static final String TABLE_NAME = "SHEET_META";
		public static final String COLUMN_NUM_OF_AT = "NUM";// number of absence
															// tables
		private static final byte INITIAL_VALUE = -1;

		public static final String CREATE_STRING = "CREATE TABLE " + TABLE_NAME
				+ " (" + COLUMN_NUM_OF_AT + " " + INTEGER_TYPE + TEXT_TYPE
				+ " )";

		public static final String DROP_TABLE = " DROP TABLE IF EXISTS "
				+ TABLE_NAME;

		public static final String Q_GET_AT_NUM = "SELECT " + COLUMN_NUM_OF_AT
				+ " FROM " + TABLE_NAME;
		/**
		 * increase the number of DBs by one
		 */
		public static final String Q_UPDATE_NUM_OF_AT = "UPDATE " + TABLE_NAME
				+ " SET " + COLUMN_NUM_OF_AT + " = " + COLUMN_NUM_OF_AT + "+1 ";

		public static final String Q_INITIAL_INSERT = "INSERT INTO "
				+ TABLE_NAME + " VALUES(" + INITIAL_VALUE + ")";

	}

	// the name of the contacts table for this list
	public static abstract class ContactsTable implements BaseColumns {
		public static final String TABLE_NAME = " CNTCTS ";
		public static final String COLUMN_ID = "ID";
		public static final String COLUMN_NAME = "NAME";
		public static final String COLUMN_PHONE_NUMBER = "PHONE_NUMBER";
		public static final String COLUMN_PHONE_NUMBER_FATHER = "PHONE_NUMBER_FATHER";
		public static final String COLUMN_PHONE_NUMBER_MOTHER = "PHONE_NUMBER_MOTHER";
		public static final String COLUMN_PHONE_NUMBER_HOME = "PHONE_NUMBER_HOME";
		public static final String COLUMN_BIRTHDAY = "BIRTHDAY";
		public static final String COLUMN_ADDRESS = "ADDRESS";
		public static final String COLUMN_IMAGE_PATH = "IMAGE_PATH";

		public static final String CREATE_STRING = "CREATE TABLE " + TABLE_NAME
				+ " (" + COLUMN_ID + " " + INTEGER_TYPE + PRIMARY_KEY
				+ COMMA_SEP + COLUMN_NAME + " " + TEXT_TYPE + COMMA_SEP
				+ COLUMN_PHONE_NUMBER + " " + TEXT_TYPE + COMMA_SEP
				+ COLUMN_PHONE_NUMBER_FATHER + " " + TEXT_TYPE + COMMA_SEP
				+ COLUMN_PHONE_NUMBER_MOTHER + " " + TEXT_TYPE + COMMA_SEP
				+ COLUMN_PHONE_NUMBER_HOME + " " + TEXT_TYPE + COMMA_SEP
				+ COLUMN_BIRTHDAY + " " + TEXT_TYPE + COMMA_SEP
				+ COLUMN_ADDRESS + " " + TEXT_TYPE + COMMA_SEP
				+ COLUMN_IMAGE_PATH + " " + TEXT_TYPE + ")";

		public static final String DROP_TABLE = "DROP TABLE IF EXISTS "
				+ TABLE_NAME;
	}


	/**
	 * 	the table that contains the names and the date of each absence table
	 * @author John
	 */
	public static abstract class MainAbsenceTable implements BaseColumns {
		public static final String TABLE_NAME = " MAIN_ABSNC_TBL ";//This Table name
		public static final String COLUMN_TABLE_CODE = "CODE";
		public static final String COLUMN_TABLE_NAME = "NAME";
		public static final String COLUMN_DATE = "DATE";

		public static final String CREATE_STRING = "CREATE TABLE " + TABLE_NAME
				+ " (" + MainAbsenceTable.COLUMN_TABLE_CODE + " " + TEXT_TYPE
				+ PRIMARY_KEY + COMMA_SEP + MainAbsenceTable.COLUMN_TABLE_NAME
				+ " " + TEXT_TYPE + COMMA_SEP + MainAbsenceTable.COLUMN_DATE
				+ " " + TEXT_TYPE + ")";

		public static final String DROP_TABLE = "DROP TABLE IF EXISTS "
				+ TABLE_NAME;
	}

	public static abstract class AbsenceTable implements BaseColumns {
		public static final String COLUMN_CONTACT_ID = "CONTACT_ID";
		public static final String COLUMN_PRESENT = "PRESENT";

		public static String getCreateString(String tableName) {
			String CREATE_STRING = "CREATE TABLE " + tableName + " ("
					+ AbsenceTable.COLUMN_CONTACT_ID + " " + INTEGER_TYPE
					+ PRIMARY_KEY + COMMA_SEP + AbsenceTable.COLUMN_PRESENT
					+ " " + INTEGER_TYPE + COMMA_SEP + " FOREIGN KEY ("
					+ AbsenceTable.COLUMN_CONTACT_ID + ") REFERENCES "
					+ ContactsTable.TABLE_NAME + " (" + ContactsTable.COLUMN_ID
					+ ") ON DELETE CASCADE)";// TODO check this one
			return CREATE_STRING;
		}

		public static String getDropQuery(String tableName) {
			String DROP_TABLE = "DROP TABLE IF EXISTS " + tableName;
			return DROP_TABLE;
		}
	}
}
