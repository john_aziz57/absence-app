package data;

/**
 * This class date won't have zero based month
 * 
 * @author John
 *
 */
public class Date {
	private int mYear = 1900;
	private int mMonth = 1;
	private int mDay = 1;

	public Date(int year, int month, int day) {
		mYear = year;
		mMonth = month;
		mDay = day;
	}
	
	public Date(Date date){
		this.mDay = date.mDay;
		this.mMonth = date.mMonth;
		this.mYear = date.mYear;
	}

	public void setDate(int year, int month, int day) {
		mYear = year;
		mMonth = month;
		mDay = day;
	}

	public int getYear() {
		return mYear;
	}

	public int getMonth() {
		return mMonth;
	}

	public int getDay() {
		return mDay;
	}
	
	@Override
	public boolean equals(Object date){
		Date temp = (Date) date;
		if(mYear!=temp.getYear() || mMonth != temp.getMonth() || mDay != temp.getDay()){
			return false;
		}
		return true;
	}
}
