package data;

import java.io.Serializable;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;

public class AbsenceContact implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String name;
	private String number;
	private String fatherNumber;
	private String motherNumber;
	private String homeNumber;
	private int ID;
	private Date birthDate;
	private String address;
	private String imagePath;
	private Bitmap image;
	private boolean imageChanged = false;

	public AbsenceContact() {
	}

	public AbsenceContact(AbsenceContact contact) {
		this.address = new String(contact.address);
		this.birthDate = new Date(contact.birthDate);
		this.fatherNumber = new String(contact.fatherNumber);
		this.homeNumber = new String(contact.homeNumber);
		this.ID = contact.ID;

		this.image = contact.getImage() != null ? Bitmap.createBitmap(contact
				.getImage()) : null;
		
		this.imageChanged = contact.imageChanged;
		this.imagePath = new String(contact.imagePath);
		this.motherNumber = new String(contact.motherNumber);
		this.name = new String(contact.name);
		this.number = new String(contact.number);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}

	public String getFatherNumber() {
		return fatherNumber;
	}

	public void setFatherNumber(String fatherNumber) {
		this.fatherNumber = fatherNumber;
	}

	public String getMotherNumber() {
		return motherNumber;
	}

	public void setMotherNumber(String motherNumber) {
		this.motherNumber = motherNumber;
	}

	public String getHomeNumber() {
		return homeNumber;
	}

	public void setHomeNumber(String homeNumber) {
		this.homeNumber = homeNumber;
	}

	public String birthdayToString() {
		if (birthDate.getMonth() < 10) {
			if (birthDate.getDay() < 10)
				return birthDate.getYear() + "0" + birthDate.getMonth() + "0"
						+ birthDate.getDay();
			return birthDate.getYear() + "0" + birthDate.getMonth() + ""
					+ birthDate.getDay();
		}
		if (birthDate.getDay() < 10) {
			return birthDate.getYear() + "" + birthDate.getMonth() + "0"
					+ birthDate.getDay();
		}
		return birthDate.getYear() + "" + birthDate.getMonth() + ""
				+ birthDate.getDay();
	}

	public Date getBirthdayDate() {
		return birthDate;
	}

	public String getFormatedBirthday() {
		// month is zero based
		return Formatter
				.getFormatedDate(birthDate.getYear(), birthDate.getMonth(),
						birthDate.getDay(), Constants.DATE_FORMAT);
	}

	/**
	 * takes paramter birthday YYYYdMMDD
	 * 
	 * @param birthday
	 */
	public void setBirthday(String birthday) {
		int year = Integer.parseInt(birthday.substring(0, 4));
		int month = Integer.parseInt(birthday.substring(4, 6));
		int day = Integer.parseInt(birthday.substring(6, 8));

		birthDate = new Date(year, month, day);
	}

	/**
	 * Set the birthday for the Contact, Month is zero based
	 * 
	 * @param year
	 * @param mon
	 * @param day
	 */
	public void setBirthday(int year, int mon, int day) {
		birthDate = new Date(year, mon, day);
	}

	public void setBirthday(Date date) {
		birthDate = date;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setImagePath(String path) {
		imagePath = path;
	}

	public String getImagePath() {
		return imagePath;
	}

	public Bitmap getImage() {
		return image;
	}

	public void setImage(Bitmap image) {
		this.image = image;
	}

	public void setNewImage(Bitmap image) {
		this.image = image;
		setImageChanged();
	}

	public boolean hasImage() {
		if ((imagePath != null && imagePath.length() != 0) || image != null) {
			return true;
		}
		return false;
	}

	public boolean hasLoadedBitmapImage() {
		if (image != null)
			return true;
		return false;
	}

	public boolean isImageChanged() {
		return imageChanged;
	}

	private void setImageChanged() {
		this.imageChanged = true;
	}

}
