package data;

import java.io.File;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;

public class ImageLoaderManager implements
		LoaderManager.LoaderCallbacks<Bitmap> {
	
	
	private File mImageFile = null;
	private final Context mContext;
	private OnImageLoadedListener mListener;
	private LoaderManager mLoaderManager;
	public ImageLoaderManager(Context context){
		mContext = context;
	}
	
	public void LoadImage(LoaderManager loaderManager,OnImageLoadedListener arg0, File imageFile) {
		mImageFile = imageFile;
		mListener = arg0;
		mLoaderManager = loaderManager;
		mLoaderManager.restartLoader(0, null, this);
	}
	
	public void cancelLoad(){
		if(mLoaderManager.hasRunningLoaders()){
			mLoaderManager.destroyLoader(0);
		}
	}

	@Override
	public Loader<Bitmap> onCreateLoader(int arg0, Bundle arg1) {
		// TODO Auto-generated method stub
		return new ImageLoader(mContext, mImageFile);
	}

	@Override
	public void onLoadFinished(Loader<Bitmap> arg0, Bitmap arg1) {
		mListener.onImageLoaded(arg1);
	}

	@Override
	public void onLoaderReset(Loader<Bitmap> arg0) {
		// TODO Auto-generated method stub

	}

	private static class ImageLoader extends AsyncTaskLoader<Bitmap> {
		private final File mImageFile;
		private boolean mResultReady = false;
		private Bitmap mResult = null;
		public ImageLoader(Context context, File imageFile) {
			super(context);
			mImageFile = imageFile;
		}

		@Override
		public Bitmap loadInBackground() {
			if (android.os.Debug.isDebuggerConnected())
				android.os.Debug.waitForDebugger();
			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inPreferredConfig = Bitmap.Config.ARGB_8888;
			Bitmap bitmap = BitmapFactory.decodeFile(
					mImageFile.getAbsolutePath(), options);
			mResultReady = true;
			return bitmap;
		}

		protected void onStartLoading() {
			if (android.os.Debug.isDebuggerConnected())
				android.os.Debug.waitForDebugger();
			if (mResultReady) {
				deliverResult(mResult);
				mResultReady = !mResultReady;
			} else {
				forceLoad();
			}
		}
		
		@Override
		public void deliverResult(Bitmap result) {
//			if(android.os.Debug.isDebuggerConnected())
//			    android.os.Debug.waitForDebugger();
			if (isReset()) {
				return;
			}
			if (isStarted())
				super.deliverResult(result);
		}


	}
	
	

}
