package data;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Scanner;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

/**
 * This class is used to import data from text file the text file will be parsed
 * line by line so that every contact is in its own line the line will be parsed
 * so that it will identify the name and the number and the birthday for
 * starters the delimiter will be a white space the name must be the first
 * string The syntax of the file was chosen to be not XML to make it easy for
 * any normal user to write its own contacts in a normal text file
 * 
 * TODO support XML the syntax of the file should be like this SheetStart Sheet:
 * ! name of the sheet ContactStart Name: Telephone: TelephoneFather:
 * TelephoneMother: TelephoneHome: Birthday:DD-MM-YYYY Address: ContactEnd
 * SheetEnd
 * 
 * @author John
 *
 */
public class SheetReader {

	private BufferedReader reader;
	public SheetReader(File file) {

		try {
			reader = new BufferedReader(new FileReader(file));

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public SheetReader(InputStream in) {

		try {
			 reader = new BufferedReader(new InputStreamReader(in));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public SheetReader(String path) {
		File file = new File(path);
		try {
			reader = new BufferedReader(new FileReader(file));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public AbsenceSheet readSheet() throws Exception {
		// check the sheet start
		if (!reader.readLine().equals(StreamConst.SH_ST)) {
			throw new SyntaxException();
		}

		AbsenceSheet sh = new AbsenceSheet();
		String line = reader.readLine();

		if (!line.substring(0, StreamConst.SH_NAME.length()).equals(StreamConst.SH_NAME)) {
			throw new SyntaxException();
		}
		String name = line.substring(StreamConst.SH_NAME.length()).trim();
		if (name.length() == 0) {
			throw new SyntaxException();
		}

		sh.setName(name);
		// start to parse the contacts
		String temp = "";
		while (reader.ready()) {// while the file reads ContactStart keep
								// reading
			temp = reader.readLine();
			if (temp.equals(StreamConst.CN_ST))
				sh.addContact(readContact(reader));
		}

		if (!temp.equals(StreamConst.SH_END)) {
			new SyntaxException();
		}
		// reading was successful
		return sh;
	}

	private AbsenceContact readContact(BufferedReader reader) throws SyntaxException, NumberFormatException, IOException {
		String temp = "";
		AbsenceContact cntct = new AbsenceContact();
		while (reader.ready()) {
			temp = reader.readLine();
			if (temp.equals(StreamConst.CN_END)) {// check if you reached the end of the
										// contact
				break;
			}
			
			int indexOf = temp.indexOf(':') + 2;// one for the empty space after the ':'
			String datatype ;
			if(indexOf != 1){// if index is zero it is ImageStart
				datatype = temp.substring(0, indexOf);
			}else{
				datatype = temp;
			}
			
			if (datatype.equals(StreamConst.CN_NAME)) {
				cntct.setName(temp.substring(indexOf).trim());
			} else if (datatype.equals(StreamConst.CN_TEL)) {
				cntct.setNumber(temp.substring(indexOf).trim());
			} else if (datatype.equals(StreamConst.CN_TEL_FA)) {
				cntct.setFatherNumber(temp.substring(indexOf).trim());
			} else if (datatype.equals(StreamConst.CN_TEL_MO)) {
				cntct.setMotherNumber(temp.substring(indexOf).trim());
			} else if (datatype.equals(StreamConst.CN_TEL_HM)) {
				cntct.setHomeNumber(temp.substring(indexOf).trim());
			} else if (datatype.equals(StreamConst.CN_BD)) {
				// TODO check if true or false
				cntct.setBirthday(temp.substring(indexOf).trim());
			} else if (datatype.equals(StreamConst.CN_ADD)) {
				cntct.setAddress(temp.substring(indexOf).trim());
			} else if (datatype.equals(StreamConst.IMAGE_START)) {
				cntct.setImage(readImage(reader));
			} else {
				throw new SyntaxException();
			}
		}

		return cntct;
	}

	private Bitmap readImage(BufferedReader reader) throws SyntaxException, NumberFormatException, IOException {
		// scan.
		String temp = reader.readLine();
		int indexOf = temp.indexOf(':') + 1;
		String datatype = temp.substring(0, indexOf);
		if (datatype.equals(StreamConst.IMAGE_WIDTH)) {
			//break;// Throw exception
			throw new SyntaxException();
		}
		int width= Integer.parseInt(temp.substring(indexOf+1));
		
		temp = reader.readLine();
		indexOf = temp.indexOf(':') + 1;
		datatype = temp.substring(0, indexOf);
		if (datatype.equals(StreamConst.IMAGE_WIDTH)) {
			//break;// Throw exception
			throw new SyntaxException();
		}
		int height= Integer.parseInt(temp.substring(indexOf+1));
		int pixelData [] = new int [width*height];
		
		temp = reader.readLine();
		indexOf = temp.indexOf(':') + 1;
		datatype = temp.substring(0, indexOf);
		if (datatype.equals(StreamConst.IMAGE_PIXEL_DATA)) {
			//break;// Throw exception
			throw new SyntaxException();
		}
		
		int index =0;
		while (reader.ready() && index<pixelData.length) {
			
			temp = reader.readLine();
//			if (temp.equals(StreamConst.IMAGE_END)) {
//				break;// should remove break and compare to the size of the image
//			} else {
				pixelData[index++]=Integer.parseInt(temp);
//			}
		}
		Bitmap image = Bitmap.createBitmap(pixelData, width, height, Bitmap.Config.ARGB_8888);
		return image;
	}

	public class SyntaxException extends Exception {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public SyntaxException() {
			super("Syntax Error Exception");
		}
	}

}