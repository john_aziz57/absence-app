package data;

/**
 * For starters this class is used to return a formated date It can be used
 * later to return other formated dates and other stuff
 * 
 * @author John
 *
 */
public class Formatter {

	private final static String[] MONTHS = { "Jan", "Feb", "Mar", "Apr", "May",
			"June", "July", "Aug", "Sept", "Oct", "Nov", "Dec" };

	private final static String[] MONTHS_FULL = { "January", "February",
			"March", "April", "May", "June", "July", "August", "September",
			"October", "November", "December" };

	// private static final String MONTH="MM";
	// private static final String FULL_MONTH="MMMM";
	// private static final String DAY="DD";
	// private static final String YEAR="YY";
	// private static final String FULL_YEAR="YYYY";
	private static final char CHAR_MONTH = 'M';
	private static final char CHAR_YEAR = 'Y';
	private static final char CHAR_DAY = 'D';

	/**
	 * The month is zero based
	 */
	public static String getFormatedDate(int year, int month, int day,
			String format) {
		StringBuilder str = new StringBuilder("");
		for (int i = 0; i < format.length();) {
			if (format.charAt(i) == CHAR_MONTH) {// It is a month
				if (format.charAt(i + 1) == CHAR_MONTH
						&& format.charAt(i + 2) == CHAR_MONTH
						&& format.charAt(i + 3) == CHAR_MONTH) {
					// it is MMMM
					str.append(MONTHS_FULL[month - 1]);//TODO check
					i += 4;
				} else if (format.charAt(i + 1) == CHAR_MONTH) {
					// it is MM
					str.append(MONTHS[month - 1]);//TODO check
					i += 2;
				} else {
					return "INVALID FORMAT";
				}

			} else if (format.charAt(i) == CHAR_DAY) {
				if (format.charAt(i + 1) == CHAR_DAY) {
					if (day < 10)
						str.append('0');
					str.append(day);
					i += 2;
				} else
					return "INVALID FORMAT";
			} else if (format.charAt(i) == CHAR_YEAR) {
				if (format.charAt(i + 1) == CHAR_YEAR
						&& format.charAt(i + 2) == CHAR_YEAR
						&& format.charAt(i + 3) == CHAR_YEAR) {
					// it is YYYY
					str.append(year);
					i += 4;
				} else if (format.charAt(i + 1) == CHAR_YEAR) {
					// it is YY
					str.append(year);
					i += 2;
				} else {
					return "INVALID FORMAT";
				}
			} else {
				str.append(format.charAt(i));
				i++;
			}
		}
		return str.toString();
	}

	public static String getFormatedDate(Date date, String format) {
		return getFormatedDate(date.getYear(), date.getMonth(), date.getDay(), format);
	}
}
