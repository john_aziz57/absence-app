package view;


import data.Constants;
import data.Date;
import data.Formatter;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;

public class DateButton extends Button{
	private static final Date DEFAULT = new Date(1991, 10, 18);
	private Date mDate;
	
	
	public DateButton(Context arg0, AttributeSet arg1){
		super(arg0, arg1);
		mDate = DEFAULT;
		this.setText(Formatter.getFormatedDate(mDate, Constants.DATE_FORMAT));
	}
	
	public DateButton(Context arg0, AttributeSet arg1,int arg2){
		super(arg0, arg1, arg2);
		mDate = DEFAULT;
		this.setText(Formatter.getFormatedDate(mDate, Constants.DATE_FORMAT));
	}
	public DateButton(Context context) {
		super(context);
		mDate = DEFAULT;
		this.setText(Formatter.getFormatedDate(mDate, Constants.DATE_FORMAT));
	}
	
	public void setDate(Date date){
		mDate=date;
		this.setText(Formatter.getFormatedDate(date, Constants.DATE_FORMAT));
	}

	
	public Date getDate()
	{
		return mDate;
	}
	
	public static void main(String[] args) {
		System.out.println(DateButton.class.getName());
	}


}
