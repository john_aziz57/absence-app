package view;

import com.john_aziz57.absence.R;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class ClickableImageView extends ImageView {
	private Context mContext;
	private String mTitle = "";

	public ClickableImageView(Context context) {
//		super(context);
		super(context,null,R.style.mini_image);

		init(context);
	}

	public ClickableImageView(Context context, AttributeSet set) {
//		super(context, set);
		super(context,set,R.style.mini_image);
		init(context);
	}

	public ClickableImageView(Context context, AttributeSet set, int num) {
//		super(context, set, num);
		super(context,set,R.style.mini_image);

		init(context);
	}
	
	private void init(Context context) {
		mContext = context;
		
		setImageResource(R.drawable.ic_personal_image);
		// Display display = getWindowManager().getDefaultDisplay();
		// int swidth = display.getWidth();
//		LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
//		setLayoutParams(lp);
//		requestLayout();
//		LayoutParams params = this.getLayoutParams();
//		params.width = 50;
//		params.height=50;
		this.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				ContactImageDialog imageView = new ContactImageDialog(mContext,
						mTitle, ((BitmapDrawable) getDrawable()).getBitmap());
				imageView.show();
			}
		});
	}
	
//	@Override
//	protected void onAttachedToWindow() {
//		// TODO Auto-generated method stub
//		super.onAttachedToWindow();
////		image_view.requestLayout();
//		LayoutParams params = this.getLayoutParams();
//		params.width = LayoutParams.MATCH_PARENT;
//		params.height = params.width;
//		this.setLayoutParams(params);
//	}

	/**
	 * set the title of the window that will show the image when clicked
	 */
	public void setTitle(String title) {
		mTitle = title;
	}

	// @Override
	// protected void onMeasure(int width, int height) {
	// super.onMeasure(width, height);
	// int measuredWidth = getMeasuredWidth();
	// int measuredHeight = getMeasuredHeight();
	// if (measuredWidth > measuredHeight) {
	// setMeasuredDimension(measuredHeight, measuredHeight);
	// } else {
	// setMeasuredDimension(measuredWidth, measuredWidth);
	//
	// }
	//
	// }

}
