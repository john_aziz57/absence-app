package view;

import com.john_aziz57.absence.R;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.widget.ImageView;

public class ContactImageDialog extends Dialog {
	private String mContactName;
	private Bitmap mContactImage;

	//
	// protected ContactImageDialog(Context context, boolean cancelable,
	// OnCancelListener cancelListener) {
	// super(context, cancelable, cancelListener);
	// // TODO Auto-generated constructor stub
	// }
	//
	// protected ContactImageDialog(Context context, int arg1) {
	// super(context, arg1);
	// // TODO Auto-generated constructor stub
	// }

	public ContactImageDialog(Context context, String contactName,
			Bitmap contactImage) {
		super(context);
		mContactName = contactName;
		mContactImage = contactImage;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTitle(mContactName);
		setContentView(R.layout.dialog_contact_image);
		ImageView contactImageView = (ImageView) findViewById(R.id.civ_iv);
		contactImageView.setImageBitmap(mContactImage);
		setCancelable(true);
	}
}
