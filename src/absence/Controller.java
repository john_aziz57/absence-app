package absence;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import com.john_aziz57.absence.R;

import data.AbsenceContact;
import data.AbsenceSheet;
import data.AbsenceTableData;
import data.DatabaseConnection;
import data.DatabaseConstants;
import data.SettingsManager;
import data.SheetWriter;
import data.DatabaseConstants.AbsenceTable;
import data.DatabaseConstants.Alpha;
import data.DatabaseConstants.AlphaMeta;
import data.DatabaseConstants.ContactsTable;
import data.DatabaseConstants.MainAbsenceTable;
import data.DatabaseConstants.SheetMeta;
import data.SheetReader;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Application;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.ContactsContract;
import android.util.DisplayMetrics;
import android.widget.EditText;
import android.widget.TextView;
//import org.acra.*;
//import org.acra.annotation.*;
import android.widget.Toast;

/**
 * The main controller
 * 
 * @author John
 * 
 */
// @ReportsCrashes(
// formKey = "", // This is required for backward compatibility but not used
// mailTo = "john_aziz57@yahoo.com",
// mode = ReportingInteractionMode.TOAST,
// resToastText = R.string.acra_report_toast
// )//ACRA TO SEND AUTOMATICALLY THE ERROR REPORTS

@SuppressLint({ "SimpleDateFormat", "UseSparseArrays" })
public class Controller extends Application {

	// private Locale locale;

	private static int time = 0;
	public static final int PICK_CONTACT = 1;

	private Context context;

	private int maxID = 0;

	/*
	 * private int currentMainID = 0; private String currentContactTable = "";
	 * private String currentMainAbsencetable = "";
	 */
	private String currentAbsenceTable = "";
	private int currentContactID = 0;
	private String currentSheetName = "";

	// private int currentContactIndex = 0;

	private ArrayList<Integer> absentID = new ArrayList<Integer>();// Index
																	// of
	// absence contact in the contacts ArrayList
	private ArrayList<Integer> presentID = new ArrayList<Integer>();// Index
																	// of
	// the present contact in the contacts ArrayList
	private AlphaDatabaseHelper alpha;
	private DatabaseConnection connect;

	private SQLiteDatabase sqlAlpha;
	private SQLiteDatabase sql;
	private ArrayList<String> absenceSheetsDB;// absence Sheets ID , that appear
												// in the
	// Main List Activity
	private ArrayList<String> absenceSheetsNames;// the names of the sheets
	private String currentDB = "";// current database opened

	private Calendar clndr = Calendar.getInstance();

	private ArrayList<Integer> contactsID;
	// contacts to speed things up a little// TODO check if this is okay to do
	// so or load when needed
	private HashMap<Integer, AbsenceContact> map;// hashmap with ID and
													// AbsenceContact
	// to help find contacts using their ID
	private ArrayList<AbsenceTableData> absenceTableData;

	private String scName = ""; // selected Contact Name
	private ArrayList<String> phones = new ArrayList<String>();// phones of the
																// selected
																// contact

	private SettingsManager sMan;

	public Controller() {
		System.out.println("Open this many " + ++time);

	}

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		// ACRA.init(this);// TO INITIATE ACRA TO SEND ERROR REPORTS
	}

	private void init() {
		absenceSheetsDB = new ArrayList<String>();
		absenceSheetsNames = new ArrayList<String>();
		// contacts = new ArrayList<AbsenceContact>();
		contactsID = new ArrayList<Integer>();
		map = new HashMap<Integer, AbsenceContact>();
		absenceTableData = new ArrayList<AbsenceTableData>();
		alpha = new AlphaDatabaseHelper(context);
		sqlAlpha = alpha.getWritableDatabase();
		loadSettings();
		// loadAbsenceSheets();
	}

	/**
	 * try to read the settings file if it wasn't found write a new one
	 */
	private void loadSettings() {
		sMan = SettingsManager.getSettingsManager(this);
		try {
			sMan.readSettings();
			setLocal(sMan.getLanguage());

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Load All the sheets of Absence, for the First Activity to Open
	 */
	private void loadAbsenceSheets() {
		String[] projection = { Alpha.COLUMN_DB_NAME, Alpha.COLUMN_SHEET_NAME };

		String sortOrder = Alpha.COLUMN_DB_NAME + " ASC";

		Cursor c = sqlAlpha.query(Alpha.TABLE_NAME, projection, null, null,
				null, null, sortOrder);
		c.moveToFirst();
		int DB_Index = c.getColumnIndexOrThrow(Alpha.COLUMN_DB_NAME);
		int Name_Index = c.getColumnIndexOrThrow(Alpha.COLUMN_SHEET_NAME);
		absenceSheetsDB.clear();
		absenceSheetsNames.clear();
		while (!c.isAfterLast()) {
			absenceSheetsDB.add(c.getString(DB_Index));
			absenceSheetsNames.add(c.getString(Name_Index));
			c.moveToNext();
		}
		c.close();
	}

	public void setContext(Context c) {
		context = this;
		init();
	}

	public void setContext() {
		context = this;
		init();
	}

	/**
	 * returns the names of the absence sheets
	 * 
	 * @return
	 */
	public String[] getAbsenceSheetsNames() {
		loadAbsenceSheets();
		if (absenceSheetsNames.size() == 0) {
			String[] names = { "" };
			return names;
		}
		String[] names = new String[absenceSheetsNames.size()];
		for (int i = 0; i < names.length; i++) {
			names[i] = absenceSheetsNames.get(i);
		}
		return names;
	}

	/**
	 * used to load the current contacts of current sheet from the database so
	 * as to respond correctly when returning names or ids of contacts NOTE must
	 * be called before getNames or getIDs
	 */
	public void loadContacts() {
		map.clear();
		contactsID.clear();
		String[] projection = {
				ContactsTable.COLUMN_ID,
				ContactsTable.COLUMN_NAME, // Name
				ContactsTable.COLUMN_PHONE_NUMBER, // own Number
				ContactsTable.COLUMN_PHONE_NUMBER_FATHER, // father's
				ContactsTable.COLUMN_PHONE_NUMBER_MOTHER, // Mother's
				ContactsTable.COLUMN_PHONE_NUMBER_HOME,
				ContactsTable.COLUMN_BIRTHDAY, ContactsTable.COLUMN_ADDRESS,
				ContactsTable.COLUMN_IMAGE_PATH }; // Home

		String sortOrder = ContactsTable.COLUMN_ID + " ASC";

		Cursor c = sql.query(ContactsTable.TABLE_NAME, projection, null, null,
				null, null, sortOrder);
		c.moveToFirst();

		while (!c.isAfterLast()) {
			AbsenceContact contact = new AbsenceContact();
			contact.setID(c.getInt(0));
			contact.setName(c.getString(1));
			contact.setNumber(c.getString(2));
			contact.setFatherNumber(c.getString(3));
			contact.setMotherNumber(c.getString(4));
			contact.setHomeNumber(c.getString(5));
			contact.setBirthday(c.getString(6));
			contact.setAddress(c.getString(7));
			contact.setImagePath(c.getString(8));
			contactsID.add(contact.getID());
			map.put(contact.getID(), contact);
			c.moveToNext();
		}
		c.close();// TODO close
		c = sql.rawQuery("SELECT MAX(" + ContactsTable.COLUMN_ID + ") FROM "
				+ ContactsTable.TABLE_NAME + "", null);
		c.moveToFirst();
		maxID = c.getInt(0);
		c.close();// TODO close
	}

	/**
	 * To get the names of the contacts, must call loadContacts() first to load
	 * data directly
	 * 
	 * @return
	 */
	public String[] getNames() {
		String[] names = new String[contactsID.size()];
		for (int i = 0; i < names.length; i++) {
			names[i] = map.get(contactsID.get(i)).getName();
		}
		return names;
	}

	/**
	 * to get the IDs of the contacts in the database, must call loadContacts()
	 * first to load data directly
	 * 
	 * @return
	 */
	public int[] getIDs() {
		int[] ids = new int[contactsID.size()];
		for (int i = 0; i < ids.length; i++) {
			ids[i] = contactsID.get(i);
		}
		return ids;
	}

	/**
	 * Return the names of the absence tables that were previously created
	 * 
	 * @return
	 */
	public String[] getAbsenceTablesNames() {
		absenceTableData.clear(); // the activity when refreshed, must use the
									// latest
		// contacts available
		String[] projection = { MainAbsenceTable.COLUMN_TABLE_CODE,
				MainAbsenceTable.COLUMN_TABLE_NAME, // Name
				MainAbsenceTable.COLUMN_DATE, };

		String sortOrder = MainAbsenceTable.COLUMN_DATE + " " + "DESC";

		Cursor c = sql.query(MainAbsenceTable.TABLE_NAME, projection, null,
				null, null, null, sortOrder);
		c.moveToFirst();

		while (!c.isAfterLast()) {
			AbsenceTableData table = new AbsenceTableData();
			table.setCode(c.getString(0));
			table.setName(c.getString(1));
			table.setDate(c.getString(2));
			absenceTableData.add(table);
			c.moveToNext();
		}
		c.close();

		String[] names = new String[absenceTableData.size()];
		for (int i = 0; i < names.length; i++) {
			names[i] = absenceTableData.get(i).getName();
			if (names[i] == null || names[i].length() == 0) {
				names[i] = absenceTableData.get(i).getFormatedDate();
			}
		}
		return names;
	}

	public void startAddContactActivity() {
		Intent i = new Intent(getBaseContext(), AddContact.class);
		i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(i);
	}

	public void startTakeAbsenceActivity() {
		Intent i = new Intent(getBaseContext(), TakeAbsenceActivity.class);
		i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(i);
	}

	public void onOptionsItemSelectedMLA(int itemID, MainListActivity activity) {
		switch (itemID) {
		case R.id.mla_ab_new_sheet:
			activity.getAlertDialog().show();
			break;
		case R.id.mla_ml_action_settings:
			startSettings();
			break;
		default:
			break;
		}
	}

	public int getNewID() {
		return ++maxID;
	}

	public int getMaxID() {
		return maxID;
	}

	// TODO remove
	// /**
	// * add contact to the database //TODO addContact should have addContact
	// * method
	// *
	// * @param activity
	// */
	// public void addContact(AddContact activity) {
	// AbsenceContact temp = new AbsenceContact();// the new absence contact
	// ContentValues values = new ContentValues();// the insert values holder
	//
	// values.put(ContactsTable.COLUMN_ID, getNewID());// get new id for
	// // contact
	// temp.setID(getMaxID());
	//
	// EditText txt = (EditText)
	// activity.findViewById(R.id.ac_et_contactName);// get
	// // Name
	// values.put(ContactsTable.COLUMN_NAME, txt.getText().toString());
	// temp.setName(txt.getText().toString());
	//
	// AutoCompleteTextView auto = (AutoCompleteTextView) activity
	// .findViewById(R.id.ac_actv_contactPhone);// get
	// // his
	// // phone
	// values.put(ContactsTable.COLUMN_PHONE_NUMBER, auto.getText().toString());
	// temp.setNumber(auto.getText().toString());
	//
	// auto = (AutoCompleteTextView) activity
	// .findViewById(R.id.ac_actv_contactPhoneFather);// get
	// // father's
	// // phone
	// values.put(ContactsTable.COLUMN_PHONE_NUMBER_FATHER, auto.getText()
	// .toString());
	// temp.setFatherNumber(auto.getText().toString());
	//
	// auto = (AutoCompleteTextView) activity
	// .findViewById(R.id.ac_actv_contactPhoneMother);// get
	// // mother's
	// // phone
	// values.put(ContactsTable.COLUMN_PHONE_NUMBER_MOTHER, auto.getText()
	// .toString());
	// temp.setMotherNumber(auto.getText().toString());
	//
	// auto = (AutoCompleteTextView) activity
	// .findViewById(R.id.ac_actv_contactPhoneHome);// get
	// // home's
	// // phone
	// values.put(ContactsTable.COLUMN_PHONE_NUMBER_HOME, auto.getText()
	// .toString());
	// temp.setHomeNumber(auto.getText().toString());
	//
	// // Insert the new row, returning the primary key value of the new row
	// sql.insert(ContactsTable.TABLE_NAME, null, values); // don't reload
	// // from database // just insert into the list of contacts
	//
	// activity.finish();
	//
	// }

	public void addContact(AbsenceContact temp) {

		ContentValues values = new ContentValues();// the insert values holder

		values.put(ContactsTable.COLUMN_ID, getNewID());// get new id for
														// contact
		values.put(ContactsTable.COLUMN_NAME, temp.getName());

		values.put(ContactsTable.COLUMN_PHONE_NUMBER, temp.getNumber());

		values.put(ContactsTable.COLUMN_PHONE_NUMBER_FATHER,
				temp.getFatherNumber());

		values.put(ContactsTable.COLUMN_PHONE_NUMBER_MOTHER,
				temp.getMotherNumber());

		values.put(ContactsTable.COLUMN_PHONE_NUMBER_HOME, temp.getHomeNumber());

		values.put(ContactsTable.COLUMN_BIRTHDAY, temp.birthdayToString());

		values.put(ContactsTable.COLUMN_ADDRESS, temp.getAddress());

		if (temp.getImage() != null) {
			File imageFile = saveImage(temp.getImage());
			values.put(ContactsTable.COLUMN_IMAGE_PATH,
					imageFile.getAbsolutePath());
		}
		// Insert the new row, returning the primary key value of the new row
		sql.insert(ContactsTable.TABLE_NAME, null, values); // don't reload
		// from database // just insert into the list of contacts

	}

	private File saveImage(Bitmap image) {

		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
				.format(new Date());
		// String timeStamp = new SimpleDateFormat("mmss").format(new Date());
		File imageFile = null;
		try {
			// imageFile = File.createTempFile(timeStamp, /* prefix */
			// ".jpg", /* suffix */
			// getFilesDir() /* directory */
			// );
			System.out.println(getFilesDir());
			System.out.println(getFilesDir());
			imageFile = new File(getFilesDir() + "/" + timeStamp + ".jpg");
			imageFile.createNewFile();

			FileOutputStream outStream = new FileOutputStream(imageFile);
			if (image.compress(Bitmap.CompressFormat.JPEG, 50, outStream)) {
				outStream.flush();
				outStream.close();
				return imageFile;
			} else
				return null;

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return imageFile;

	}

	/**
	 * This method is used to terminate every activity
	 * 
	 * @param activity
	 */
	public void cancel(Activity activity) {
		activity.finish();

	}

	/**
	 * return a new Code for the Database to be named the code is just a number
	 * incremented every time a database is created
	 * 
	 * @return
	 */
	private int getNewDBCode() {
		sqlAlpha.execSQL(AlphaMeta.Q_UPDATE_NUM_OF_DB);// increase the code of
														// DB

		Cursor c = sqlAlpha.rawQuery(AlphaMeta.Q_GET_DB_NUM, null);// get the
																	// new got
																	// that was
		// just incremented
		c.moveToFirst();
		int code = c.getInt(0);
		c.close();
		return code;
	}

	/**
	 * Called when the dialog prompts the new name of the list makes the new
	 * sheet is the current open one
	 * 
	 * @param newSheetName
	 *            the new name of the list
	 */
	public void createNewAbsenceSheet(String newSheetName) {

		currentDB = DatabaseConstants.DB_PREFIX + (getNewDBCode()) + ".db";

		ContentValues values = new ContentValues();
		values.put(Alpha.COLUMN_DB_NAME, currentDB);
		values.put(Alpha.COLUMN_SHEET_NAME, newSheetName);

		sqlAlpha.insert(Alpha.TABLE_NAME, null, values);
		// finished the insert into alpha

		// open connection to the new database
		if (connect != null) {
			connect.close();// TODO watch out
		}
		if (sql != null) {
			sql.close(); // TODO watch out
		}
		connect = new DatabaseConnection(context, currentDB);
		sql = connect.getWritableDatabase();// table used will be created
											// automatically

		// adding the new sheet to the current data
		absenceSheetsNames.add(newSheetName);
		absenceSheetsDB.add(currentDB);

		// View the sheet
		startViewSheet();
	}

	public void importSheet(AbsenceSheet sheet) {
		// TODO check the name of the sheet isn't repeated
		currentDB = DatabaseConstants.DB_PREFIX + (getNewDBCode()) + ".db";

		ContentValues values = new ContentValues();
		values.put(Alpha.COLUMN_DB_NAME, currentDB);
		values.put(Alpha.COLUMN_SHEET_NAME, sheet.getSheetName());

		sqlAlpha.insert(Alpha.TABLE_NAME, null, values);
		// finished the insert into alpha

		// open connection to the new database
		if (connect != null) {
			connect.close();// TODO watch out
		}
		if (sql != null) {
			sql.close(); // TODO watch out
		}
		connect = new DatabaseConnection(context, currentDB);
		sql = connect.getWritableDatabase();// table used will be created
											// automatically

		// adding the new sheet to the current data
		absenceSheetsNames.add(sheet.getSheetName());
		absenceSheetsDB.add(currentDB);

		for (int i = 0; i < sheet.getSheetSize(); i++) {
			addContact(sheet.getContact(i));
		}
		Toast.makeText(getApplicationContext(),
				"Imported " + sheet.getSheetName() + " Sheet",
				Toast.LENGTH_LONG).show();
		// View the sheet //TODO view the list of sheets and refresh it after
		// import
		startViewSheet();
	}

	public void importSheet(String path) {
		SheetReader reader = new SheetReader(path);
		AbsenceSheet as = null;
		try {
			as = reader.readSheet();
		} catch (Exception e) {
			Toast.makeText(
					getApplicationContext(),
					"Error occured while importing the file,"
							+ "Check the file format.", Toast.LENGTH_LONG)
					.show();
			return;
		}
		// TODO check the name of the sheet isn't repeated
		currentDB = DatabaseConstants.DB_PREFIX + (getNewDBCode()) + ".db";

		ContentValues values = new ContentValues();
		values.put(Alpha.COLUMN_DB_NAME, currentDB);
		values.put(Alpha.COLUMN_SHEET_NAME, as.getSheetName());

		sqlAlpha.insert(Alpha.TABLE_NAME, null, values);
		// finished the insert into alpha

		// open connection to the new database
		if (connect != null) {
			connect.close();// TODO watch out
		}
		if (sql != null) {
			sql.close(); // TODO watch out
		}
		connect = new DatabaseConnection(context, currentDB);
		sql = connect.getWritableDatabase();// table used will be created
											// automatically

		// adding the new sheet to the current data
		absenceSheetsNames.add(as.getSheetName());
		absenceSheetsDB.add(currentDB);

		for (int i = 0; i < as.getSheetSize(); i++) {
			addContact(as.getContact(i));
		}
		Toast.makeText(getApplicationContext(),
				"Imported " + as.getSheetName() + " Sheet", Toast.LENGTH_LONG)
				.show();
		// View the sheet //TODO view the list of sheets and refresh it after
		// import
		startViewSheet();
	}

	public void exportSheet(String path) {
		AbsenceSheet sheet = new AbsenceSheet();
		sheet.setName(currentSheetName);
		for (AbsenceContact value : map.values()) {
			sheet.addContact(value);
		}
		SheetWriter shW = new SheetWriter(new File(path), sheet);
		shW.writeSheet();
	}

	public void onSheetClicktMLA(int index) {
		currentDB = absenceSheetsDB.get(index);
		currentSheetName = absenceSheetsNames.get(index);

		connect = new DatabaseConnection(context, currentDB);
		sql = connect.getWritableDatabase();
		// should start the main activity
		startViewSheet();
	}

	public String getCurrentSheetName() {
		return currentSheetName;
	}

	public void onCloseingSheet() {
		sql.close();
	}

	public void onCloseAll() {
		alpha.close();
	}

	public void startEditContact() {
		Intent i = new Intent(getBaseContext(), EditContactActivity.class);
		i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(i);
	}

	public void startViewSheet() {
		Intent i = new Intent(getBaseContext(), ViewSheetActivity.class);
		i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(i);
	}

	/**
	 * When a contact is selected view its details on the EditContactActivity
	 * 
	 * @param ID
	 *            the id of the selected contact
	 */
	public void onSelectContact(int ID) {
		currentContactID = ID;
		Intent i = new Intent(getBaseContext(), ViewContactActivity.class);
		i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(i);
	}

	/**
	 * return by value
	 * 
	 * @return
	 */
	public AbsenceContact getCurrentContact() {
		return new AbsenceContact(map.get(currentContactID));
	}

	/**
	 * @param editedContact
	 *            : the contact after editing
	 */
	public void editContact(AbsenceContact editedContact) {
		ContentValues values = new ContentValues();// the insert values holder

		values.put(ContactsTable.COLUMN_NAME, editedContact.getName());

		values.put(ContactsTable.COLUMN_PHONE_NUMBER, editedContact.getNumber());

		values.put(ContactsTable.COLUMN_PHONE_NUMBER_FATHER,
				editedContact.getFatherNumber());

		values.put(ContactsTable.COLUMN_PHONE_NUMBER_MOTHER,
				editedContact.getMotherNumber());

		values.put(ContactsTable.COLUMN_PHONE_NUMBER_HOME,
				editedContact.getHomeNumber());

		values.put(ContactsTable.COLUMN_BIRTHDAY,
				editedContact.birthdayToString());

		values.put(ContactsTable.COLUMN_ADDRESS, editedContact.getAddress());

		if (editedContact.isImageChanged()) {
			if (getCurrentContact().getImagePath() != null) {
				File oldImageFile = new File(getCurrentContact().getImagePath());
				oldImageFile.delete();
			}
			File newImageFile = saveImage(editedContact.getImage());
			values.put(ContactsTable.COLUMN_IMAGE_PATH,
					newImageFile.getAbsolutePath());
			editedContact.setImagePath(newImageFile.getAbsolutePath());
		}
		// Insert the new row, returning the primary key value of the new row
		// sql.insert(currentContactTable, null, values); // don't reload
		String[] args = { currentContactID + "" };

		sql.beginTransaction();
		try {
			sql.update(ContactsTable.TABLE_NAME, values,
					ContactsTable.COLUMN_ID + "= ?", args);
			sql.setTransactionSuccessful();
		} finally {
			sql.endTransaction();
		}

		map.remove(editedContact.getID());
		map.put(editedContact.getID(), new AbsenceContact(editedContact));
		// AbsenceContact temp = map.get(contact.getID());
		// temp=contact;// this should do it;

		// from database // just insert into the list of contacts

	}

	public void saveContact(EditContactActivity activity) {

		ContentValues values = new ContentValues();// the insert values holder

		EditText txt = (EditText) activity.findViewById(R.id.ec_et_contactName);// get
																				// Name
		values.put(ContactsTable.COLUMN_NAME, txt.getText().toString());

		txt = (EditText) activity.findViewById(R.id.ec_et_contactPhone);// get
																		// his
																		// phone
		values.put(ContactsTable.COLUMN_PHONE_NUMBER, txt.getText().toString());

		txt = (EditText) activity.findViewById(R.id.ec_et_contactPhoneFather);// get
																				// father's
																				// phone
		values.put(ContactsTable.COLUMN_PHONE_NUMBER_FATHER, txt.getText()
				.toString());

		txt = (EditText) activity.findViewById(R.id.ec_et_contactPhoneMother);// get
																				// mother's
																				// phone
		values.put(ContactsTable.COLUMN_PHONE_NUMBER_MOTHER, txt.getText()
				.toString());

		txt = (EditText) activity.findViewById(R.id.ec_et_contactPhoneHome);// get
																			// home's
																			// phone
		values.put(ContactsTable.COLUMN_PHONE_NUMBER_HOME, txt.getText()
				.toString());

		// Insert the new row, returning the primary key value of the new row
		// sql.insert(currentContactTable, null, values); // don't reload
		String[] args = { currentContactID + "" };
		sql.update(ContactsTable.TABLE_NAME, values, ContactsTable.COLUMN_ID
				+ "= ?", args);
		// from database // just insert into the list of contacts
		activity.finish();
	}

	private int getNewATCode() {
		sql.execSQL(SheetMeta.Q_UPDATE_NUM_OF_AT);// update the number of
													// Absence table to be ready
													// to get the
		// next code
		Cursor c = sql.rawQuery(SheetMeta.Q_GET_AT_NUM, null);// get the a new
																// code for the
																// absence table
		c.moveToFirst();
		int code = c.getInt(0);
		c.close();
		return code;
	}

	public void absenceDone(boolean[] attendance) {
		// TODO remove activity parameter
		// TODO take the present and absence array as parameters
		currentAbsenceTable = DatabaseConstants.ABSENCE_TABLE_PREFIX + ""
				+ (getNewATCode());// create table with new code i.e. AT14 AT67

		ContentValues values = new ContentValues();

		values.put(MainAbsenceTable.COLUMN_TABLE_CODE, currentAbsenceTable);

		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddkkmm");
		clndr = Calendar.getInstance();
		// System.out.println(formatter.format(clndr.getTime()));
		values.put(MainAbsenceTable.COLUMN_DATE,
				formatter.format(clndr.getTime()));

		sql.insert(MainAbsenceTable.TABLE_NAME, null, values);
		// Done Adding info about the Absence table
		// create the absence table
		sql.execSQL(AbsenceTable.getCreateString(currentAbsenceTable));
		presentID.clear();
		absentID.clear();
		// insert data into it
		for (int i = 0; i < contactsID.size(); i++) {
			if (attendance[i]) {
				String query = "insert into " + currentAbsenceTable
						+ " VALUES (" + contactsID.get(i)
						+ DatabaseConstants.COMMA_SEP
						+ DatabaseConstants.BOOLEAN_TRUE + ")";

				sql.execSQL(query);
				presentID.add(contactsID.get(i));

			} else {
				sql.execSQL("insert into " + currentAbsenceTable + " VALUES ("
						+ contactsID.get(i) + DatabaseConstants.COMMA_SEP
						+ DatabaseConstants.BOOLEAN_FALSE + ")");
				absentID.add(contactsID.get(i));// put the index of the contact
												// in the
				// contacts
				// array list to reference it fast
			}
		}

		Intent i = new Intent(getBaseContext(), ViewAbsenceActivity.class);
		i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

		startActivity(i);
	}

	public String[] getAbsentNames() {
		String[] names = new String[absentID.size()];
		for (int i = 0; i < names.length; i++) {
			names[i] = map.get(absentID.get(i)).getName();
		}
		return names;
	}

	public int[] getAbsentID() {
		int[] ids = new int[absentID.size()];
		for (int i = 0; i < ids.length; i++) {
			ids[i] = absentID.get(i);
		}
		return ids;
	}

	public String[] getPresentNames() {
		String[] names = new String[presentID.size()];
		for (int i = 0; i < names.length; i++) {
			names[i] = map.get(presentID.get(i)).getName();
		}
		return names;
	}

	public int[] getPresentID() {
		int[] ids = new int[presentID.size()];
		for (int i = 0; i < ids.length; i++) {
			ids[i] = presentID.get(i);
		}
		return ids;
	}

	/**
	 * Absent Contacts arg2 | | | | | | | | --> |contact| |3 | ----> |2 |--| | |
	 * | | | | | | | | | | | | ----- He is number 3 in Absent List, and He is
	 * number in contacts
	 * 
	 * @param id
	 *            the index of the absent in the absent list
	 */
	public void onAbsentClick(int index) {
		onSelectContact(absentID.get(index));
	}

	public void onPresentClick(int index) {
		onSelectContact(presentID.get(index));
	}

	public void onNumberClicked(TextView ed) {
		Intent phoneIntent = new Intent(Intent.ACTION_DIAL);
		phoneIntent.setData(Uri.parse("tel:" + ed.getText().toString()));
		phoneIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(phoneIntent);
	}

	/**
	 * When an absence table table is selected
	 * 
	 * @param arg2
	 */
	public void onSelectAbsenceTable(int index) {
		currentAbsenceTable = absenceTableData.get(index).getCode();
		Cursor c = sql.rawQuery("SELECT " + AbsenceTable.COLUMN_CONTACT_ID
				+ "," + AbsenceTable.COLUMN_PRESENT + " FROM "
				+ currentAbsenceTable, null);

		presentID.clear();
		absentID.clear();
		// insert data into it
		while (c.moveToNext()) {
			if (c.getInt(1) == DatabaseConstants.BOOLEAN_TRUE) {
				presentID.add(c.getInt(0));

			} else {
				absentID.add(c.getInt(0));// put the index of the contact in the
			}
		}

		c.close();
		Intent i = new Intent(getBaseContext(), ViewAbsenceActivity.class);
		i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

		startActivity(i);
	}

	// // TODO check if used
	// public void importContact(AddContact addContact) {
	// Intent intent = new Intent(Intent.ACTION_PICK,
	// ContactsContract.Contacts.CONTENT_URI);
	// intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK |
	// Intent.FLAG_ACTIVITY_SINGLE_TOP);
	// // | Intent.FLAG_ACTIVITY_SINGLE_TOP
	// addContact.startActivityForResult(intent, PICK_CONTACT);
	// }

	// what does it do ?//TODO
	public void onActivityResultAC(AddContact addContact, int resultCode,
			Intent data) {

		if (resultCode == Activity.RESULT_OK) {
			phones.clear();
			Uri contactData = data.getData();

			String id = contactData.getLastPathSegment();

			// select the name
			Cursor cursor = getContentResolver().query(contactData, null, null,
					null, null);
			cursor.moveToFirst();

			scName = cursor
					.getString(cursor
							.getColumnIndexOrThrow(ContactsContract.Contacts.DISPLAY_NAME));

			Cursor phoneCur = getContentResolver().query(
					ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
					ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
					new String[] { id }, null);

			while (phoneCur.moveToNext()) {
				// This would allow you get several phone addresses
				// if the phone addresses were stored in an array
				String phone = phoneCur
						.getString(phoneCur
								.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DATA));
				phones.add(phone);
				phones.trimToSize();
			}
			phoneCur.close();
			cursor.close();
		}

	}

	public String getSelectedName() {
		return scName;

	}

	public ArrayList<String> getSelectedPhones() {
		return phones;
	}

	/**
	 * Start the settings activity
	 */
	public void startSettings() {
		Intent settings = new Intent(this, settings.Settings.class);
		settings.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(settings);

	}

	/**
	 * Change the current local and restart the settings activity
	 * 
	 * @param lang
	 */
	public void changeLangSettings(String lang) {
		sMan.setLanguage(lang);// setting it locally
		setLocal(sMan.getLanguage());
		// startSettings();
	}

	public String getLanguage() {
		return sMan.getLanguage();
	}

	private void setLocal(String lang) {
		sMan.setLanguage(lang);
		Locale locale = new Locale(lang);
		Resources res = getResources();
		DisplayMetrics dm = res.getDisplayMetrics();
		Configuration conf = res.getConfiguration();
		conf.locale = locale;
		res.updateConfiguration(conf, dm);
	}

	public void saveSettings() {

		sMan.writeSettings();
	}

	public void deleteContacts(boolean[] delete) {
		for (int i = 0; i < delete.length; i++) {
			if (delete[i]) {
				// delete from all the lists
				sql.delete(ContactsTable.TABLE_NAME, ContactsTable.COLUMN_ID
						+ "=?", new String[] { contactsID.get(i) + "" });
			}
		}
	}

	public void deleteSheets(boolean[] selected) {
		for (int i = 0; i < selected.length; i++) {
			if (selected[i]) {
				// TODO check if working
				sqlAlpha.delete(Alpha.TABLE_NAME, Alpha.COLUMN_DB_NAME + "=?",
						new String[] { absenceSheetsDB.get(i) + "" });
				this.deleteDatabase(absenceSheetsDB.get(i));
			}
		}
	}

	public void deleteAbsenceTables(boolean[] selected) {
		for (int i = 0; i < selected.length; i++) {
			if (selected[i]) {
				// TODO check if working
				sql.delete(MainAbsenceTable.TABLE_NAME,
						MainAbsenceTable.COLUMN_TABLE_CODE + "=?",
						new String[] { absenceTableData.get(i).getCode() });
				sql.execSQL(AbsenceTable.getDropQuery(absenceTableData.get(i)
						.getCode()));
			}
		}
	}

	/**
	 * This method calls bluetoothSendActivity
	 */
	public void sendSheet() {

		AbsenceSheet sheet = new AbsenceSheet();
		sheet.setName(currentSheetName);
		for (AbsenceContact value : map.values()) {
			sheet.addContact(value);
		}

		Intent i = new Intent(this, bluetooth.BluetoothSendActivity.class);
		i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		i.putExtra(data.Constants.BT_SHEET, sheet);
		startActivity(i);
	}

	public void receiveSheet() {

	}

	// TODO to return to the app after a call
	/*
	 * private class PhoneCallListener extends PhoneStateListener {
	 * 
	 * private boolean isPhoneCalling = false;
	 * 
	 * String LOG_TAG = "LOGGING 123";
	 * 
	 * @Override public void onCallStateChanged(int state, String
	 * incomingNumber) {
	 * 
	 * if (TelephonyManager.CALL_STATE_RINGING == state) { // phone ringing
	 * Log.i(LOG_TAG, "RINGING, number: " + incomingNumber); }
	 * 
	 * if (TelephonyManager.CALL_STATE_OFFHOOK == state) { // active
	 * Log.i(LOG_TAG, "OFFHOOK");
	 * 
	 * isPhoneCalling = true; }
	 * 
	 * if (TelephonyManager.CALL_STATE_IDLE == state) { // run when class
	 * initial and phone call ended, need detect flag // from CALL_STATE_OFFHOOK
	 * Log.i(LOG_TAG, "IDLE");
	 * 
	 * if (isPhoneCalling) {
	 * 
	 * Log.i(LOG_TAG, "restart app");
	 * 
	 * // restart app Intent i = getBaseContext().getPackageManager()
	 * .getLaunchIntentForPackage( getBaseContext().getPackageName());
	 * i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); startActivity(i);
	 * 
	 * isPhoneCalling = false; }
	 * 
	 * } } }
	 */
}
