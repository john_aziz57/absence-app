package absence;

import com.john_aziz57.absence.R;
import com.john_aziz57.myfilechooser.MainActivity;
import com.john_aziz57.myfilechooser.MainPresenter;

import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBar.Tab;
import android.support.v7.app.ActionBarActivity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TabHost;

/**
 * This class has the two fragments, one to view the contacts and the other to
 * view the absence tables taken before
 * 
 * @author John
 *
 */
public class ViewSheetActivity extends ActionBarActivity implements
		ActionBar.TabListener {

	private static final int FOLDER_SELECT_CODE = 0;
	private Controller ctrl;
	private TabHost mTabHost;
	private ViewPager viewPager;
	// TODO Add TAbseAdapter
	private TabsAdapter mTabsAdapter;
	private ActionBar actionBar;
	@SuppressWarnings("unused")
	private int currentTab = 0;// default
	private ViewContactsFragment vcf;
	private ViewAbsenceTablesFragment vatf;
	// Tab titles
	private String[] tabs;// TODO get from controller

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_sheet);
		ctrl = (Controller) getApplicationContext();

		mTabHost = (TabHost) findViewById(android.R.id.tabhost);
		

		mTabHost.setup();

		viewPager = (ViewPager) findViewById(R.id.pager); 
		actionBar = getSupportActionBar();
		mTabsAdapter = new TabsAdapter(getSupportFragmentManager());

		viewPager.setAdapter(mTabsAdapter);
		actionBar.setHomeButtonEnabled(false);
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		tabs = new String[] { getResources().getString(R.string.tab_names),
				getResources().getString(R.string.tabs_absnc_tbl) };
		for (String tab_name : tabs) {
			actionBar.addTab(actionBar.newTab().setText(tab_name)
					.setTabListener(this));
		}
		// When swiping between different sections, select the corresponding
		// tab. We can also use ActionBar.Tab#select() to do this if we have
		// a reference to the Tab.
		viewPager
				.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
					@Override
					public void onPageSelected(int position) {
						currentTab = position;
						actionBar.setSelectedNavigationItem(position);
					}
				});

		setTitle(ctrl.getCurrentSheetName()); // set the title
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		ctrl.onCloseingSheet();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		// ctrl.onOptionsItemSelectedMA(id);
		switch (id) {
		case R.id.action_add:
			ctrl.startAddContactActivity();
			break;
		case R.id.ma_action_take_absence:
			ctrl.startTakeAbsenceActivity();
			break;
		case R.id.ma_export_sheet:
			exportSheet();
			break;
		case R.id.ma_send_sheet:
			ctrl.sendSheet();
			break;

		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	private void exportSheet() {
		// we should get a path to export to.
		Intent i = new Intent(this,
				com.john_aziz57.myfilechooser.MainActivity.class);
		i.putExtra(MainPresenter.TAG, MainPresenter.SELECT_FOLDER);
		startActivityForResult(i, FOLDER_SELECT_CODE);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
		case FOLDER_SELECT_CODE:
			if (resultCode == RESULT_OK) {

				String path = data.getStringExtra(MainActivity.RESULT);
				if (path != null)
					ctrl.exportSheet(path);

			}
			break;
		}
		// super.onActivityResult(requestCode, resultCode, data);
	}

	// TODO should use broadcast pattern
	@Override
	public void onBackPressed() {
		if (!vcf.onBackButtonPressed() && !vatf.onBackButtonPressed()) {
			super.onBackPressed();
		}
	}

	private class TabsAdapter extends FragmentPagerAdapter {

		public TabsAdapter(android.support.v4.app.FragmentManager fm) {
			super(fm);
		}

		@Override
		public android.support.v4.app.Fragment getItem(int index) {
			currentTab = index;
			switch (index) {
			case 0:
				// view contacts fragment activity
				vcf = new ViewContactsFragment(ctrl);
				return vcf;
			case 1:
				// Absence tables fragment activity
				vatf = new ViewAbsenceTablesFragment(ctrl);
				return vatf;
			}

			return null;
		}

		@Override
		public int getCount() {
			// get item count - equal to number of tabs
			return 2;
		}

	}

	@Override
	public void onTabReselected(Tab arg0,
			android.support.v4.app.FragmentTransaction arg1) {
		viewPager.setCurrentItem(arg0.getPosition());
		// When the given tab is selected, switch to the corresponding page in
		// the ViewPager.

	}

	@Override
	public void onTabSelected(Tab arg0,
			android.support.v4.app.FragmentTransaction arg1) {
		viewPager.setCurrentItem(arg0.getPosition());

	}

	@Override
	public void onTabUnselected(Tab arg0,
			android.support.v4.app.FragmentTransaction arg1) {
		// TODO Auto-generated method stub

	}

}
