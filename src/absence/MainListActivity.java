package absence;

import java.net.URISyntaxException;

import view.DateButton;

import com.john_aziz57.absence.R;
import com.john_aziz57.myfilechooser.MainActivity;
import com.john_aziz57.myfilechooser.MainPresenter;

import data.AbsenceSheet;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.view.ActionMode;
import android.support.v7.view.ActionMode.Callback;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class MainListActivity extends ActionBarActivity {
	private static final int FILE_SELECT_CODE = 0;
	private final static int RECEIVE_FILE = 2;
	private Controller ctrl;
	private ArrayAdapter<String> adapter;
	private AlertDialog alertDialog;
	private EditText userInput;
	private String language;

	private ListView list;
	private SelectAdapter selectAdapter;
	private boolean selectState = false;// to indicate whether I am in select
										// state or not
	private ActionMode am = null;// used to calling contextual menu
	private Callback actionModeCallBack = new Callback() {

		@Override
		public boolean onPrepareActionMode(ActionMode arg0, Menu arg1) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public void onDestroyActionMode(ActionMode arg0) {
			endSelectState();
		}

		@Override
		public boolean onCreateActionMode(ActionMode arg0, Menu arg1) {
			// selectState = true;//TODO WAS
			MenuInflater inflater = arg0.getMenuInflater();
			inflater.inflate(R.menu.ml_cab, arg1);
			return true;
		}

		@Override
		public boolean onActionItemClicked(ActionMode arg0, MenuItem arg1) {
			switch (arg1.getItemId()) {
			case R.id.ml_cab_delete:
				// send the selected contacts to the controller

				ctrl.deleteSheets(selectAdapter.getSelected());
				break;
			default:// TODO need to refactor this place
				// endSelectState();
				am.finish();
			}
			if (am != null) {
				am.finish();
			}
			return true;
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		
		setContentView(R.layout.activity_main_list);
		ctrl = (Controller) getApplicationContext();
		ctrl.setContext();

		list = (ListView) findViewById(R.id.mla_listView1);
		adapter = new ArrayAdapter<String>(this, R.layout.simple_list_item_1,
				ctrl.getAbsenceSheetsNames());
		list.setAdapter(adapter);

		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				ctrl.onSheetClicktMLA(arg2);
			}
		});
		list.setLongClickable(true);
		list.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				startSelectState();
				return true;
			}
		});

		// / creating the dialog
		LayoutInflater li = LayoutInflater.from(this);
		View promptsView = li.inflate(R.layout.dialog_new_list, null);
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

		alertDialogBuilder.setView(promptsView);
		userInput = (EditText) promptsView.findViewById(R.id.dla_et_new_name);
		alertDialogBuilder
				.setCancelable(false)
				.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						// get user input and set it to result
						// edit text
						ctrl.createNewAbsenceSheet(userInput.getText()
								.toString());
					}
				})
				.setNegativeButton("Cancel",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						});
		// create alert dialog
		alertDialog = alertDialogBuilder.create();
		language = ctrl.getLanguage();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		ctrl.onCloseAll();
	}

	@Override
	public void onResume() {
		super.onResume();
		if (!ctrl.getLanguage().equals(language)) {
			finish();
			startActivity(getIntent());
		}
		refreshSheets();
	}

	public void refreshSheets() {
		ListView list = (ListView) findViewById(R.id.mla_listView1);
		adapter = new ArrayAdapter<String>(this, R.layout.simple_list_item_1,
				ctrl.getAbsenceSheetsNames());
		list.setAdapter(adapter);
	}

	@SuppressLint("InlinedApi")
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main_list, menu);

		MenuItem locationItem = menu.findItem(R.id.mla_ab_new_sheet);
		locationItem.setIcon(R.drawable.ic_action_new);
		MenuItemCompat.setShowAsAction(locationItem,
				MenuItem.SHOW_AS_ACTION_IF_ROOM);

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// the option menu selection
		int id = item.getItemId();
		// ctrl.onOptionsItemSelectedMLA(id, this);
		switch (id) {
		case R.id.mla_ab_new_sheet:
			getAlertDialog().show();
			break;
		case R.id.mla_ml_action_settings:
			ctrl.startSettings();
			break;
		case R.id.mla_import_sheet:
			showFileChooser();
			break;
		case R.id.mla_receive_sheet:
			receiveSheet();
		default:
			break;
		}
		return true;
	}

	/**
	 * Start the activity that will receive the sheet through Bluetooth
	 */
	private void receiveSheet() {
		Intent i = new Intent(this, bluetooth.BluetoothReceiveActivity.class);
		//i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivityForResult(i, RECEIVE_FILE);		
	}

	public AlertDialog getAlertDialog() {
		return alertDialog;
	}

	@Override
	public void onBackPressed() {
		if (selectState) {
			endSelectState();
		} else {
			super.onBackPressed();
		}

	}

	/**
	 * Select state is the state which shows the checkbox and allows selection
	 * this method starts the select state
	 */
	private void startSelectState() {
		selectAdapter = new SelectAdapter(this, ctrl.getAbsenceSheetsNames());
		list.setAdapter(selectAdapter);
		selectState = true;
		am = this.startSupportActionMode(actionModeCallBack);
	}

	/**
	 * Select state is the state which shows a check box next to the name THIS
	 * METHOD endS Select state
	 */
	private void endSelectState() {
		selectState = false;
		adapter = new ArrayAdapter<String>(this, R.layout.simple_list_item_1,
				ctrl.getAbsenceSheetsNames());

		list.setAdapter(adapter);
		am = null;
	}

	// File Chooser
	private void showFileChooser() {

		Intent i = new Intent(this,
				com.john_aziz57.myfilechooser.MainActivity.class);
		i.putExtra(MainPresenter.TAG, MainPresenter.SELECT_FILE);
		startActivityForResult(i, FILE_SELECT_CODE);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent resultData) {
		switch (requestCode) {
		case FILE_SELECT_CODE:
			if (resultCode == RESULT_OK) {
				
				String path =  resultData.getStringExtra(MainActivity.RESULT);

				if (path != null)
					ctrl.importSheet(path);
			}
			break;
		case RECEIVE_FILE:
			if (resultCode == RESULT_OK) {
				ctrl.importSheet((AbsenceSheet)resultData.getSerializableExtra(data.Constants.BT_SHEET));
			}
			break;
		}
		//super.onActivityResult(requestCode, resultCode, data);
	}


}
