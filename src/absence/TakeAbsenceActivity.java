package absence;


import com.john_aziz57.absence.R;
import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

public class TakeAbsenceActivity extends ActionBarActivity {
	
	private Controller ctrl;
	private TakeAbsenceAdapter adapter;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_take_absence);
		ctrl = (Controller) getApplicationContext();
		
		ListView list  = (ListView) findViewById(R.id.ta_listView1);
		ctrl.loadContacts();// to load the contacts and get the latest contacts available
		adapter = new TakeAbsenceAdapter(this,ctrl.getNames());
		list.setAdapter(adapter);
		
	}

	@SuppressLint("InlinedApi")
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.take_absence, menu);

		MenuItem locationItem = menu.findItem( R.id.ta_ab_done);
        locationItem.setIcon(R.drawable.ic_action_done);
        
        MenuItemCompat.setShowAsAction(locationItem, MenuItem.SHOW_AS_ACTION_IF_ROOM);
        
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		switch(item.getItemId()){
//		case R.id.action_settings: 
//			return true;
		case R.id.ta_ab_done:
			//ctrl.absenceDone(this);
			ctrl.absenceDone(adapter.getAttendance());
			/**
			 * The absence is sent to the controller through the adapter, every time a person is 
			 * check or unchecked the adapter informs the controller
			 * TODO no need to send an istance of the activity to the controller
			 */
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
}
