package absence;

public interface SelectableFragmentInterface {
	
	public boolean onBackButtonPressed();
	public void setIndex(int index);
	public int getIndex();
	public void startSelection();
	public void endSelection();
	public boolean inSelectionState();
}
