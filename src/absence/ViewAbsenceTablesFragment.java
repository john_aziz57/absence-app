package absence;

import com.john_aziz57.absence.R;

import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.view.ActionMode;
import android.support.v7.view.ActionMode.Callback;
//import android.support.v7.view.ActionMode;
//import android.support.v7.view.ActionMode.Callback;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
//import android.view.Menu;
//import android.view.MenuInflater;
//import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
//import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class ViewAbsenceTablesFragment extends AbstractSelectableFragment {
	private Controller ctrl;
	private ArrayAdapter<String> adapter;

	private ListView list;
	private int index;
	
	private SelectAdapter selectAdapter;
	private boolean selectState = false;
	
	private ActionMode am=null;
	private Callback actionModeCallBack  = new Callback() {
		
		@Override
		public boolean onPrepareActionMode(ActionMode arg0, Menu arg1) {
			return false;
		}
		
		@Override
		public void onDestroyActionMode(ActionMode arg0) {
			endSelectState();
		}
		
		@Override
		public boolean onCreateActionMode(ActionMode arg0, Menu arg1) {
			MenuInflater inflater = arg0.getMenuInflater();
			inflater.inflate(R.menu.vat_cab, arg1);//TODO not that menu
			return true;
		}
		
		@Override
		public boolean onActionItemClicked(ActionMode arg0, MenuItem arg1) {
			switch(arg1.getItemId()){
				case R.id.vcs_cab_delete:
					
					ctrl.deleteAbsenceTables(selectAdapter.getSelected());
					break;
				default:
					am.finish();//am.finish() calls onDesteryActionMode()
			}
			if(am!=null){
				am.finish();
			}
			return true;
		}
	};

	
	public ViewAbsenceTablesFragment(Controller controller) {
		ctrl = controller;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_view_absence_tables,
				container, false);

		list = (ListView) rootView.findViewById(R.id.vatf_list);

		// TODO remove contacts loading from creation, put it in onResumed()
		adapter = new ArrayAdapter<String>(getActivity(),
				R.layout.simple_list_item_1,
				ctrl.getAbsenceTablesNames());
		list.setAdapter(adapter);

		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				ctrl.onSelectAbsenceTable(arg2);
			}
		});

		list.setLongClickable(true);
		list.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				startSelectState();
				return true;
			}
		});

		return rootView;
	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}
	@Override
	public void onResume() {
		super.onResume();
		refreshAbsenceTables();// TODO make sure it is optimal
	}

	public void refreshAbsenceTables() {
		// TODO refresh contacts creates new adapter. this should be altered
		ListView list = (ListView) getView().findViewById(R.id.vatf_list);
		adapter = new ArrayAdapter<String>(getActivity(),
				R.layout.simple_list_item_1,
				ctrl.getAbsenceTablesNames());
		list.setAdapter(adapter);
	}

	public boolean inSelectState() {
		return false;
	}

	/**
	 * to return true when in select status
	 * else false 
	 */
	public boolean onBackButtonPressed() {
		if(selectState){
			endSelectState();
			return true;
		}
		return false;
	}
	

	
	@Override
	public void setIndex(int index) {
		this.index = index;
	}

	@Override
	public int getIndex() {
		return index;
	}
	
	
	private void startSelectState(){
		selectAdapter = new SelectAdapter(getActivity(), ctrl
				.getAbsenceTablesNames());
		list.setAdapter(selectAdapter);
		selectState = true;
		am = ((ActionBarActivity)(getActivity())).startSupportActionMode( actionModeCallBack);
	}
	
	private void endSelectState(){
		selectState=false;
		ctrl.loadContacts();
		adapter = new ArrayAdapter<String>(getActivity(),
				R.layout.simple_list_item_1, ctrl.getAbsenceTablesNames());

		list.setAdapter(adapter);
		am=null;
	}

	@Override
	public void startSelection() {
		startSelectState();
	}

	@Override
	public void endSelection() {
		endSelectState();
		
	}

	@Override
	public boolean inSelectionState() {
		return selectState;
	}
	
}
