package absence;

import java.io.File;

import view.DateButton;

import com.john_aziz57.absence.R;

import data.AbsenceContact;
import data.Constants;
import data.Date;
import data.Formatter;
import data.ImageLoaderManager;
import data.OnImageLoadedListener;
//import absence.AddContact.DatePickerFragment;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.MediaStore.MediaColumns;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;

public class EditContactActivity extends ActionBarActivity implements
		OnImageLoadedListener {

	protected static final int REQUEST_CAMERA = 0;
	protected static final int SELECT_IMAGE = 1;
	protected static final int SELECT_CONTACT = 2;

	private Controller ctrl;
	private AbsenceContact currentContact;
	private DateButton btn;
	private ImageView imageView = null;
	private Bitmap bitmapImage = null;
	private boolean newImageFlag = false;
	private boolean imageLoaded = false;
	ImageLoaderManager imageLoader = null;

	// private String birthday = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edit_contact);
		ctrl = (Controller) getApplicationContext();
		// ctrl.loadCurrentContact(this);

		init();
	}

	// @Override// faulty logic restarts all the fields
	// protected void onResume() {
	// super.onResume();
	// init();
	//
	// }

	private void init() {
		currentContact = ctrl.getCurrentContact();

		setTitle(currentContact.getName());

		EditText txt = (EditText) this.findViewById(R.id.ec_et_contactName);// get
		// Name
		txt.setText(currentContact.getName());

		txt = (EditText) this.findViewById(R.id.ec_et_contactPhone);// get
		// his
		// phone
		txt.setText(currentContact.getNumber());

		txt = (EditText) this.findViewById(R.id.ec_et_contactPhoneFather);// get
		// father's
		// phone
		txt.setText(currentContact.getFatherNumber());

		txt = (EditText) this.findViewById(R.id.ec_et_contactPhoneMother);// get
		// mother's
		// phone
		txt.setText(currentContact.getMotherNumber());

		txt = (EditText) this.findViewById(R.id.ec_et_contactPhoneHome);// get
		// home's
		// phone
		txt.setText(currentContact.getHomeNumber());

		btn = (DateButton) this.findViewById(R.id.ec_btn_birthday);
		btn.setDate(currentContact.getBirthdayDate());

		txt = (EditText) this.findViewById(R.id.ec_et_address);// get address
		txt.setText(currentContact.getAddress());

		imageView = (ImageView) findViewById(R.id.ec_iv_image);
		imageView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				selectImage();
			}
		});
		bitmapImage = BitmapFactory.decodeResource(getResources(),
				R.drawable.ic_personal_image);
		if (currentContact.hasImage()) {
			if (currentContact.hasLoadedBitmapImage()) {
				imageView.setImageBitmap(currentContact.getImage());
			} else {
				ImageLoaderManager imageLoader = new ImageLoaderManager(this);
				imageLoader.LoadImage(getSupportLoaderManager(), this,
						new File(currentContact.getImagePath()));
			}
		}

	}

	@SuppressLint("InlinedApi")
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.edit_contact, menu);

		// save contact item
		MenuItem locationItem = menu.findItem(R.id.ec_ab_save);
		locationItem.setIcon(R.drawable.ic_action_save);
		MenuItemCompat.setShowAsAction(locationItem,
				MenuItem.SHOW_AS_ACTION_IF_ROOM);

		return true;
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.ec_ab_save) {
			editContact();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void editContact() {

		EditText txt = (EditText) findViewById(R.id.ec_et_contactName);// get
		currentContact.setName(txt.getText().toString()); // Name

		txt = (EditText) findViewById(R.id.ec_et_contactPhone);// get
																// his
																// phone
		currentContact.setNumber(txt.getText().toString());

		txt = (EditText) findViewById(R.id.ec_et_contactPhoneFather);// get//
																		// father's//
																		// phone
		currentContact.setFatherNumber(txt.getText().toString());

		txt = (EditText) findViewById(R.id.ec_et_contactPhoneMother);// get//
																		// mother's//
																		// phone
		currentContact.setMotherNumber(txt.getText().toString());

		txt = (EditText) findViewById(R.id.ec_et_contactPhoneHome);// get home's
																	// phone
		currentContact.setHomeNumber(txt.getText().toString());

		// year , month, day , zerobased

		currentContact.setBirthday(btn.getDate());

		txt = (EditText) findViewById(R.id.ec_et_address);// get address
		currentContact.setAddress(txt.getText().toString());

		if (newImageFlag)
			currentContact.setNewImage(bitmapImage);

		ctrl.editContact(currentContact);
		finish();
	}

	public class DatePickerFragment extends DialogFragment implements
			OnDateSetListener {

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {

			Date date = currentContact.getBirthdayDate();

			// Create a new instance of DatePickerDialog and return it
			return new DatePickerDialog(getActivity(), this, date.getYear(),
					date.getMonth() - 1, date.getDay());
		}

		@Override
		public void onDateSet(DatePicker arg0, int year, int month, int day) {
			btn.setDate(new Date(year, month + 1, day));
			// btn.setDate()(Formatter.getFormatedDate(year, month, day, true,
			// Constants.DATE_FORMAT));
		}
	}

	public void setBirthday(View v) {
		DialogFragment newFragment = new DatePickerFragment();
		newFragment.show(getSupportFragmentManager(), "datePicker");
	}

	@Override
	public void onImageLoaded(Bitmap image) {
		if (image != null && !imageLoaded) {
			bitmapImage= image;
			imageView.setImageBitmap(bitmapImage);
			imageLoaded = true;
		}// else {
		//	imageView.setImageBitmap(bitmapImage);
		//}

	}

	private void selectImage() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Select Image Source");
		final CharSequence items[] = { "Open Gallery", "Open Camera", "Cancel" };
		builder.setItems(items, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface arg0, int item) {
				if (items[item].equals("Open Camera")) {
					Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
					startActivityForResult(intent, REQUEST_CAMERA);
				} else if (items[item].equals("Open Gallery")) {
					Intent intent = new Intent(
							Intent.ACTION_PICK,
							android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
					intent.setType("image/*");
					startActivityForResult(
							Intent.createChooser(intent, "Select File"),
							SELECT_IMAGE);
				} else if (items[item].equals("Cancel")) {
					arg0.dismiss();
				}
			}

		});
		builder.show();

	}

	@SuppressLint("NewApi")
	public void onActivityResult(int reqCode, int resultCode, Intent data) {
		super.onActivityResult(reqCode, resultCode, data);
		switch (reqCode) {
		case REQUEST_CAMERA:
			onCameraReturned(resultCode, data);
			break;
		case SELECT_IMAGE:
			onImageSelected(resultCode, data);
			break;
		}

	}

	private void onImageSelected(int resultCode, Intent data) {

		if (resultCode == RESULT_OK) {
			newImageFlag = true;
			if (imageLoader != null)
				imageLoader.cancelLoad();
			Uri selectedImageUri = data.getData();
			String[] projection = { MediaColumns.DATA };
			Cursor cursor = managedQuery(selectedImageUri, projection, null,
					null, null);
			int column_index = cursor.getColumnIndexOrThrow(MediaColumns.DATA);
			cursor.moveToFirst();
			String selectedImagePath = cursor.getString(column_index);
			bitmapImage = BitmapFactory.decodeFile(selectedImagePath);
			imageView.setImageBitmap(bitmapImage);
			currentContact.setImage(bitmapImage);

		}
	}

	private void onCameraReturned(int resultCode, Intent data) {
		if (resultCode == RESULT_OK) {
			newImageFlag = true;
			if (imageLoader != null)
				imageLoader.cancelLoad();
			Bundle extras = data.getExtras();
			bitmapImage = (Bitmap) extras.get("data");
			imageView.setImageBitmap(bitmapImage);
		}
	}

}
