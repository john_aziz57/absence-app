package absence;

import com.john_aziz57.absence.R;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

public class SelectAdapter extends BaseAdapter {

	private final Activity c;
	private boolean[] selected;
	private String[] itemsNames;

	public SelectAdapter(Activity context, String[] toSelectItems) {
		c = context;
		itemsNames = toSelectItems;
		selected = new boolean[itemsNames.length];
	}

	@Override
	public int getCount() {
		return itemsNames.length;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View customView, ViewGroup arg2) {
		LayoutInflater inflater = c.getLayoutInflater();
		// TODO line below change to the view sent as a parameter
		customView = inflater.inflate(R.layout.select_row, null, true);

		TextView txtName = (TextView) customView.findViewById(R.id.sc_tv_name);
		final CheckBox cb = (CheckBox) customView
				.findViewById(R.id.sc_cb_select);

		txtName.setText(itemsNames[position]);

		LinearLayout ll = (LinearLayout) customView
				.findViewById(R.id.sc_ll_row);
		ll.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				selected[position] = !selected[position];
				cb.setChecked(selected[position]);
			}
		});
		
		cb.setChecked(selected[position]);
		return customView;
	}

	public boolean[] getSelected() {
		return selected;
	}

}
