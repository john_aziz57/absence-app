package absence;

import com.john_aziz57.absence.R;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

public class TakeAbsenceAdapter extends BaseAdapter {
	private final Activity context;
	private String[] contacts;
	private boolean attendance[];

	public TakeAbsenceAdapter(Activity context, String[] contacts) {
		this.context = context;
		this.contacts = contacts;
		attendance = new boolean [contacts.length];
	}
	
	// change it to pool if possible
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		//TODO switch context with ctrl
		LayoutInflater inflater = context.getLayoutInflater();
		View customView = inflater.inflate(R.layout.select_row, null, true);

		TextView cntctName = (TextView) customView
				.findViewById(R.id.sc_tv_name);
		//cntctName.setTextSize(size);
		// cntctName.setTextSize(Controller.FONT_SIZE);

		final CheckBox cb = (CheckBox) customView
				.findViewById(R.id.sc_cb_select);

		cntctName.setText(contacts[position]);
		LinearLayout ll = (LinearLayout) customView.findViewById(R.id.sc_ll_row);
		
		ll.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				attendance[position] = !attendance[position];
				cb.setChecked(attendance[position]);// if the checkbox was unchecked
				// set it to check// TODO set a local array and then send the data at the
				// end
//				ctrl.setPresent(position, checkBox.isChecked());
			}
		});
		cb.setChecked(attendance[position]);
		return customView;
	}

	@Override
	public int getCount() {
		return contacts.length;
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}
	
	public boolean [] getAttendance(){
		return attendance;
	}
}
