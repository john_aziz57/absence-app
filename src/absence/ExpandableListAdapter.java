package absence;

import com.john_aziz57.absence.R;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseExpandableListAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * This Expandable list will have only one group
 * 
 * @author John
 *
 */
public class ExpandableListAdapter extends BaseExpandableListAdapter {
	private Controller ctrl;
	private Activity c;
	private String[][] names;
	private int IDs[][];
	private String[] header;

	/**
	 * 
	 * @param inContext
	 *            context for layout inflation
	 * @param inCtrl
	 *            the Controller to handle when the contact is selected
	 * @param inHeader
	 *            the headers of the expandable list should be "Absent"
	 *            "Present"
	 * @param inNames
	 *            Names in 2D array first hold the absent, the second holds the
	 *            present
	 * @param inIDs
	 *            IDs in 2D array the first holds the ID of the absent and the
	 *            second holds the ID for the present
	 */
	public ExpandableListAdapter(Activity inContext, Controller inCtrl,
			String[] inHeader, String[][] inNames, int[][] inIDs) {
		c = inContext;
		ctrl = inCtrl;
		names = inNames;
		IDs = inIDs;
		header = inHeader;
	}

	@Override
	public Object getChild(int arg0, int arg1) {
		return names[arg0][arg1];
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {

		LayoutInflater infalInflater = c.getLayoutInflater();
		convertView = infalInflater.inflate(R.layout.va_elv_item, null);
		TextView txt = (TextView) convertView.findViewById(R.id.va_elv_tv_item);
		txt.setText(names[groupPosition][childPosition]);

		LinearLayout ll = (LinearLayout) convertView
				.findViewById(R.id.va_elv_ll_item);
		ll.setOnClickListener(new CustomListener(
				IDs[groupPosition][childPosition]));
		return convertView;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return names[groupPosition].length;
	}

	@Override
	public Object getGroup(int groupPosition) {
		return header[groupPosition];
	}

	@Override
	public int getGroupCount() {
		return header.length;
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		if (convertView == null) {
			LayoutInflater infalInflater = c.getLayoutInflater();
			convertView = infalInflater.inflate(R.layout.va_elv_header, null);
		}

		TextView lblListHeader = (TextView) convertView
				.findViewById(R.id.va_elv_tv_header);
		// lblListHeader.setTypeface(null, Typeface.BOLD);
		lblListHeader.setText(header[groupPosition]);

		return convertView;
	}

	@Override
	public boolean hasStableIds() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return true;
	}

	private class CustomListener implements OnClickListener {
		private int id = 0;

		public CustomListener(int inId) {
			id = inId;
		}

		@Override
		public void onClick(View v) {
			ctrl.onSelectContact(id);
		}
	}

}
