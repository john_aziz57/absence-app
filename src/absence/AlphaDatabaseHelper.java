package absence;




import data.DatabaseConstants.Alpha;
import data.DatabaseConstants.AlphaMeta;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class AlphaDatabaseHelper extends SQLiteOpenHelper{
	public static final String DATABASE_NAME = "alpha.db";
	public static final int DATABASE_VERSION =  1;
	public AlphaDatabaseHelper(Context context){
		super(context,DATABASE_NAME,null,1);
	}
	
	@Override
	public void onCreate(SQLiteDatabase arg0) {
		arg0.execSQL(Alpha.DROP_TABLE);
		arg0.execSQL(Alpha.CREATE_STRING);
		arg0.execSQL(AlphaMeta.DROP_TABLE);
		arg0.execSQL(AlphaMeta.CREATE_QUERY);
		arg0.execSQL(AlphaMeta.Q_INITIAL_INSERT);// insert the inital value 
		// which is zero
		
	}
	@Override
	public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub
	}
}
