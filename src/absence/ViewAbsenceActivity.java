package absence;

import com.john_aziz57.absence.R;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ExpandableListView;

public class ViewAbsenceActivity extends Activity {
	private Controller ctrl;
 
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_absence);
		ctrl = (Controller) getApplicationContext();
		
		final String headers[]= new String[]{getString(R.string.va_absent),getString(R.string.va_present)};
		final String names[][]= new String[headers.length][];
		names[0]=ctrl.getAbsentNames();
		names[1]=ctrl.getPresentNames();
		
		final int IDs[][] = new int [headers.length][];
		IDs[0]= ctrl.getAbsentID();
		IDs[1]= ctrl.getPresentID();
		
		// ABSENT
		ExpandableListView elvA = (ExpandableListView) findViewById(R.id.va_elv_absent);

		ExpandableListAdapter elvAdapterA = new ExpandableListAdapter(this,
				ctrl, headers, names,
				IDs);
		elvA.setAdapter(elvAdapterA);
		elvA.expandGroup(0);
		elvA.expandGroup(1);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.view_absence, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
