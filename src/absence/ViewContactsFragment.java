package absence;

import com.john_aziz57.absence.R;

import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.view.ActionMode;
import android.os.Bundle;
import android.support.v7.view.ActionMode.Callback;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
//import android.widget.Toast;

public class ViewContactsFragment extends AbstractSelectableFragment {

	private Controller ctrl;
	private ArrayAdapter<String> adapter;
	private ListView list;
	private int index;
	private int [] ids;
	
	private SelectAdapter selectAdapter;	
	private boolean selectState = false;// to indicate whether I am in select state or not	
	private ActionMode am=null;
	private Callback actionModeCallBack  = new Callback() {
		
		@Override
		public boolean onPrepareActionMode(ActionMode arg0, Menu arg1) {
			// TODO Auto-generated method stub
			return false;
		}
		
		@Override
		public void onDestroyActionMode(ActionMode arg0) {
			endSelection();
			//			selectState=false;//TODO WAS
//			am=null; // no idea why this line// TODO			
		}
		
		@Override
		public boolean onCreateActionMode(ActionMode arg0, Menu arg1) {
//			selectState = true;//TODO WAS
			MenuInflater inflater = arg0.getMenuInflater();
			inflater.inflate(R.menu.vcs_cab, arg1);
			return true;
		}
		
		@Override
		public boolean onActionItemClicked(ActionMode arg0, MenuItem arg1) {
			switch(arg1.getItemId()){
				case R.id.vcs_cab_delete: 
					// send the selected contacts to the controller
					
					ctrl.deleteContacts(((SelectAdapter)(list.getAdapter())).getSelected());
					break;
				default://TODO need to refactor this place
					am.finish();
			}
			if(am!=null){
				am.finish();
			}
			return true;
		}
	};

	
	public ViewContactsFragment(Controller controller) {
		ctrl = controller;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_view_contacts,
				container, false);
		list = (ListView) rootView.findViewById(R.id.vcf_list);

		// TODO remove contacts loading from creation, put it in onResumed()
		ctrl.loadContacts();// to get the latest contacts and load them so as to
							// get the names
		// need to make load contacts a separate operation
		ids =  ctrl.getIDs();
		adapter = new ArrayAdapter<String>(getActivity(),
				R.layout.simple_list_item_1, ctrl.getNames());

		list.setAdapter(adapter);
		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				ctrl.onSelectContact(ids[arg2]);
			}
		});
		
		// TODO ON LONG CLICK
		list.setLongClickable(true);
		list.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				startSelection();
				return true;
			}
		});

		return rootView;
	}
	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}
	@Override
	public void onResume() {
		super.onResume();
		refreshContacts();// TODO make sure it is optimal
		// Toast.makeText(getActivity(), "Fragment Resumed",
		// Toast.LENGTH_SHORT).show();
	}

	public void refreshContacts() {

		// TODO refresh contacts creates new adapter. this should be altered
		ListView list = (ListView) getView().findViewById(R.id.vcf_list);
		ctrl.loadContacts();
		ids =  ctrl.getIDs();
		adapter = new ArrayAdapter<String>(getActivity(),
				R.layout.simple_list_item_1, ctrl.getNames());
		list.setAdapter(adapter);
	}

	public boolean inSelectState() {
		// TODO Check if used
		return selectState;
	}

	/**
	 * to return true only when it is in selectState
	 */
	public boolean onBackButtonPressed() {

		if(selectState){
			endSelection();
			return true;
		}
		return false;
	}

	@Override
	public void setIndex(int index) {
		this.index = index;
	}

	@Override
	public int getIndex() {
		return index;
	}
	
	public void startSelection(){
		selectAdapter = new SelectAdapter(getActivity(), ctrl
				.getNames());
		list.setAdapter(selectAdapter);
		selectState = true;
		am = ((ActionBarActivity)(getActivity())).startSupportActionMode( actionModeCallBack);
	}
	
	public void endSelection(){
		selectState=false;
		ctrl.loadContacts();
		adapter = new ArrayAdapter<String>(getActivity(),
				R.layout.simple_list_item_1, ctrl.getNames());

		list.setAdapter(adapter);
		am=null;
	}

	@Override
	public boolean inSelectionState() {
		return selectState;
	}

}
