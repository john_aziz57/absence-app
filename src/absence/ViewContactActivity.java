package absence;

import java.io.File;

import view.ClickableImageView;

import com.john_aziz57.absence.R;

import data.AbsenceContact;
import data.ImageLoaderManager;
import data.OnImageLoadedListener;
import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class ViewContactActivity extends ActionBarActivity implements
		OnImageLoadedListener {
	private Controller ctrl;
	private AbsenceContact currentContact;
	private boolean onCreate = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_view_contact);
		ctrl = (Controller) getApplicationContext();
		onCreate = true;
		init();

	}

	@Override
	protected void onResume() {
		super.onResume();
		if (!onCreate )//|| onPause)// the first time the user opens the activity
			// the initiate method will be called twice
			// we need the initiate method to be only called when
			// editContactActivity
			// comes forward to load the changes done to the contact
			init();
		onCreate = false;
	}
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	private void init() {

		currentContact = ctrl.getCurrentContact();
		setTitle(currentContact.getName());// set the title for the activity

		TextView txt = (TextView) this.findViewById(R.id.vc_et_contactName);// get
		// Name
		txt.setText(currentContact.getName());

		txt = (TextView) this.findViewById(R.id.vc_et_contactPhone);// get
		// his
		// phone
		if (currentContact.getNumber().length() == 0) {
			txt.setVisibility(View.GONE);
			txt = (TextView) this.findViewById(R.id.vc_tv_textView01);// get
			txt.setVisibility(View.GONE);
		} else {
			txt.setText(currentContact.getNumber());
			txt.setOnClickListener(new CustomListener(txt));
		}
		
		txt = (TextView) this.findViewById(R.id.vc_et_contactPhoneFather);// get
		// father's
		// phone
		if (currentContact.getFatherNumber().length() == 0) {
			txt.setVisibility(View.GONE);
			txt = (TextView) this.findViewById(R.id.vc_tv_textView02);// get
			txt.setVisibility(View.GONE);

		} else {
			txt.setText(currentContact.getFatherNumber());
			txt.setOnClickListener(new CustomListener(txt));
		}


		txt = (TextView) this.findViewById(R.id.vc_et_contactPhoneMother);// get
		// mother's
		// phone
		if (currentContact.getMotherNumber().length() == 0) {
			txt.setVisibility(View.GONE);
			txt = (TextView) this.findViewById(R.id.vc_tv_textView03);// get
			txt.setVisibility(View.GONE);

		} else {
			txt.setText(currentContact.getMotherNumber());
			txt.setOnClickListener(new CustomListener(txt));		
		}


		txt = (TextView) this.findViewById(R.id.vc_et_contactPhoneHome);// get
		// home's
		// phone
		if (currentContact.getHomeNumber().length() == 0) {
			txt.setVisibility(View.GONE);
			txt = (TextView) this.findViewById(R.id.vc_tv_textView04);// get
			txt.setVisibility(View.GONE);

		} else {
			txt.setText(currentContact.getHomeNumber());
			txt.setOnClickListener(new CustomListener(txt));		}



		
		
		txt = (TextView) this.findViewById(R.id.vc_tv_birthday);// get birthday
		if (currentContact.getBirthdayDate()!= null) {
			txt.setVisibility(View.GONE);
			txt = (TextView) this.findViewById(R.id.vc_tv_textView05);// get
			txt.setVisibility(View.GONE);

		} else {
			txt.setText(currentContact.getFormatedBirthday());
		}

		
		txt = (TextView) this.findViewById(R.id.vc_tv_address);// get birthday
		if (currentContact.getAddress().length() == 0) {
			txt.setVisibility(View.GONE);
			txt = (TextView) this.findViewById(R.id.vc_tv_textView06);// get
			txt.setVisibility(View.GONE);

		} else {
			txt.setText(currentContact.getAddress());		
		}
		
		// image
		if (currentContact.hasImage()) {
			ImageLoaderManager imageLoader = new ImageLoaderManager(this);
			imageLoader.LoadImage(getSupportLoaderManager(), this, new File(
					currentContact.getImagePath()));
		}

	}

	@SuppressLint("InlinedApi")
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.view_contact, menu);

		MenuItem locationItem = menu.findItem(R.id.vc_ab_edit);
		locationItem.setIcon(R.drawable.ic_action_edit);

		MenuItemCompat.setShowAsAction(locationItem,
				MenuItem.SHOW_AS_ACTION_IF_ROOM);

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		switch (item.getItemId()) {
		case R.id.action_settings:
			return true;
		case R.id.vc_ab_edit:
			ctrl.startEditContact();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	// TODO check not used
	public void onNumberClick(View v) {

		TextView txt = (TextView) v;
		ctrl.onNumberClicked(txt);
	}

	private class CustomListener implements OnClickListener {
		private TextView txt;

		public CustomListener(TextView v) {
			txt = v;
		}

		@Override
		public void onClick(View v) {

			ctrl.onNumberClicked(txt);
		}

	}

	@Override
	public void onImageLoaded(Bitmap image) {
		ClickableImageView imageView = (ClickableImageView) this
				.findViewById(R.id.vc_iv_image);
		if (image != null) {// error loading the image
			imageView.setImageBitmap(image);
			imageView.setTitle(currentContact.getName());
		}
	}
}
