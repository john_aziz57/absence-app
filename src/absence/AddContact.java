package absence;

import java.util.ArrayList;
import java.util.Calendar;

import com.john_aziz57.absence.R;

import data.AbsenceContact;
import data.Constants;
import data.Formatter;
import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.provider.MediaStore.MediaColumns;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;

public class AddContact extends ActionBarActivity {

	protected static final int REQUEST_CAMERA = 0;
	protected static final int SELECT_IMAGE = 1;
	protected static final int SELECT_CONTACT = 2;
	private Controller ctrl;
	private Button btn;
	// private String birthday="19911118";
	private int byear = 1991, bmon = 11, bday = 18;
	private ImageView imageView = null;
	private Bitmap bitmapImage = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_contact);
		ctrl = (Controller) getApplicationContext();
		btn = (Button) findViewById(R.id.ac_btn_birthday);
		imageView = (ImageView) findViewById(R.id.ac_iv_image);
		imageView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				selectImage();
			}
		});

	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		imageView = null;
		bitmapImage = null;
	}
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.add_contact, menu);

		// save contact icon
		MenuItem locationItem = menu.add(0, R.id.ac_ab_save, 0, R.string.save);
		locationItem.setIcon(R.drawable.ic_action_save);
		MenuItemCompat.setShowAsAction(locationItem,
				MenuItem.SHOW_AS_ACTION_IF_ROOM);

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		switch (item.getItemId()) {
		case R.id.action_settings:
			return true;
		case R.id.ac_ab_save:
			// ctrl.addContact(this);
			addContact();
		}
		return super.onOptionsItemSelected(item);
	}

	private void addContact() {
		AbsenceContact temp = new AbsenceContact();// the new absence contact //
													// contact

		EditText txt = (EditText) findViewById(R.id.ac_et_contactName);// get
																		// Name
		temp.setName(txt.getText().toString());

		AutoCompleteTextView auto = (AutoCompleteTextView) findViewById(R.id.ac_actv_contactPhone);// get
		// his
		// phone
		temp.setNumber(auto.getText().toString());

		auto = (AutoCompleteTextView) findViewById(R.id.ac_actv_contactPhoneFather);// get
		// father's
		// phone
		temp.setFatherNumber(auto.getText().toString());

		auto = (AutoCompleteTextView) findViewById(R.id.ac_actv_contactPhoneMother);// get
		// mother's
		// phone
		temp.setMotherNumber(auto.getText().toString());

		auto = (AutoCompleteTextView) findViewById(R.id.ac_actv_contactPhoneHome);// get
		// home's
		// phone
		temp.setHomeNumber(auto.getText().toString());
		// year, month,day, zero based
		temp.setBirthday(byear, bmon, bday);

		txt = (EditText) findViewById(R.id.ac_et_address);
		temp.setAddress(txt.getText().toString());

		// temp.setImage(((BitmapDrawable)imageView.getDrawable()).getBitmap());
		temp.setImage(bitmapImage);
		ctrl.addContact(temp);
		finish();

	}

	public void importContact(View v) {
		// This call must be done from here not from Controller
		Intent intent = new Intent(Intent.ACTION_PICK,
				ContactsContract.Contacts.CONTENT_URI);
		startActivityForResult(intent, SELECT_CONTACT);
	}

	@SuppressLint("NewApi")
	public void onActivityResult(int reqCode, int resultCode, Intent data) {
		super.onActivityResult(reqCode, resultCode, data);

		switch (reqCode) {
		case REQUEST_CAMERA:
			onCameraReturned(resultCode, data);
			break;
		case SELECT_IMAGE:
			onImageSelected(resultCode, data);
			break;
		case SELECT_CONTACT:
			onContactSelected(resultCode, data);
			break;
		}
	}

	private void onImageSelected(int resultCode, Intent data) {
		if (resultCode == RESULT_OK) {
			Uri selectedImageUri = data.getData();
			String[] projection = { MediaColumns.DATA };
			Cursor cursor = managedQuery(selectedImageUri, projection, null,
					null, null);
			int column_index = cursor.getColumnIndexOrThrow(MediaColumns.DATA);
			cursor.moveToFirst();
			String selectedImagePath = cursor.getString(column_index);
			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inJustDecodeBounds = true;
			// BitmapFactory.decodeFile(selectedImagePath, options);
			// final int REQUIRED_SIZE = 200;
			// int scale = 1;
			// while (options.outWidth / scale / 2 >= REQUIRED_SIZE
			// && options.outHeight / scale / 2 >= REQUIRED_SIZE)
			// scale *= 2;
			// options.inSampleSize = scale;
			// options.inJustDecodeBounds = false;
			bitmapImage = BitmapFactory.decodeFile(selectedImagePath);
			imageView.setImageBitmap(bitmapImage);
		}
	}

	private void onCameraReturned(int resultCode, Intent data) {
		if (resultCode == RESULT_OK) {
			Bundle extras = data.getExtras();
			bitmapImage = (Bitmap) extras.get("data");
			imageView.setImageBitmap(bitmapImage);
		}
	}

	private void onContactSelected(int resultCode, Intent data) {
		if (resultCode == RESULT_OK) {
			ctrl.onActivityResultAC(this, resultCode, data);
			String scName = ctrl.getSelectedName();
			TextView txt = (TextView) findViewById(R.id.ac_et_contactName);
			txt.setText(scName);

			// set the auto complete with those results
			ArrayList<String> phones = ctrl.getSelectedPhones();

			AutoCompleteTextView auto = (AutoCompleteTextView) findViewById(R.id.ac_actv_contactPhone);
			ArrayAdapter<String> ad = new ArrayAdapter<String>(this,
					R.layout.dropdown_layout, phones);
			auto.setAdapter(ad);
			auto.setOnFocusChangeListener(new CustomOnFocusChangedListener(auto));

			auto = (AutoCompleteTextView) findViewById(R.id.ac_actv_contactPhoneFather);
			ad = new ArrayAdapter<String>(this, R.layout.dropdown_layout,
					phones);
			auto.setAdapter(ad);
			auto.setOnFocusChangeListener(new CustomOnFocusChangedListener(auto));

			auto = (AutoCompleteTextView) findViewById(R.id.ac_actv_contactPhoneMother);
			ad = new ArrayAdapter<String>(this, R.layout.dropdown_layout,
					phones);
			auto.setAdapter(ad);
			auto.setOnFocusChangeListener(new CustomOnFocusChangedListener(auto));

			auto = (AutoCompleteTextView) findViewById(R.id.ac_actv_contactPhoneHome);
			ad = new ArrayAdapter<String>(this, R.layout.dropdown_layout,
					phones);
			auto.setAdapter(ad);
			auto.setOnFocusChangeListener(new CustomOnFocusChangedListener(auto));
		}

	}

	private class CustomOnFocusChangedListener implements OnFocusChangeListener {
		private AutoCompleteTextView auto;

		public CustomOnFocusChangedListener(AutoCompleteTextView input) {
			auto = input;
		}

		@Override
		public void onFocusChange(View v, boolean hasFocus) {
			if (hasFocus) {
				auto.showDropDown();
			}

		}

	}

	public class DatePickerFragment extends DialogFragment implements
			OnDateSetListener {

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			// Use the current date as the default date in the picker
			final Calendar c = Calendar.getInstance();
			int year = c.get(Calendar.YEAR);
			int month = c.get(Calendar.MONTH);
			int day = c.get(Calendar.DAY_OF_MONTH);

			// Create a new instance of DatePickerDialog and return it
			return new DatePickerDialog(getActivity(), this, year, month, day);
		}

		@Override
		public void onDateSet(DatePicker arg0, int year, int month, int day) {
			byear = year;
			bmon = month;
			bday = day;
			btn.setText(Formatter.getFormatedDate(year, month, day,
					Constants.DATE_FORMAT));
		}
	}

	public void setBirthday(View v) {
		DialogFragment newFragment = new DatePickerFragment();
		newFragment.show(getSupportFragmentManager(), "datePicker");
	}

	private void selectImage() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle("Select Image Source");
		final CharSequence items[] = { "Open Gallery", "Open Camera", "Cancel" };
		builder.setItems(items, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface arg0, int item) {
				if (items[item].equals("Open Camera")) {
					Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
					if (intent.resolveActivity(getPackageManager()) != null) 
						startActivityForResult(intent, REQUEST_CAMERA);
				} else if (items[item].equals("Open Gallery")) {
					Intent intent = new Intent(
							Intent.ACTION_PICK,
							android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
					intent.setType("image/*");
					startActivityForResult(
							Intent.createChooser(intent, "Select File"),
							SELECT_IMAGE);
				} else if (items[item].equals("Cancel")) {
					arg0.dismiss();
				}
			}

		});
		builder.show();

	}
}
