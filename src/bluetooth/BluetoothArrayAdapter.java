package bluetooth;

import java.util.ArrayList;



import com.john_aziz57.absence.R;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class BluetoothArrayAdapter extends BaseAdapter{
	private final ArrayList<BluetoothDevice> list = new ArrayList<BluetoothDevice>();
	private final LayoutInflater mInflater;
	
	public BluetoothArrayAdapter(Context context) {
		 mInflater = LayoutInflater.from(context);
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public BluetoothDevice getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}
	
	public void addItem(BluetoothDevice device){
		if(list.size()<1)
		list.add(device);
		else{
			for(int i =0;i<list.size();i++){
				if(list.get(i).equals(device)){
					return;
				}
			}
			list.add(device);
		}
		notifyDataSetChanged();
	}
	
    public void insert(BluetoothDevice device, int index) {
        list.add(index, device);
        notifyDataSetChanged();
    }


	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;

        if (row == null)// he is checking if the row is made or not
            row = mInflater.inflate(R.layout.bluetooth_device_row, parent, false);
       
        TextView txt = (TextView) row.findViewById(R.id.tv_bluetooth_device);

        // Get the file at the current position
        final BluetoothDevice device = getItem(position);

        // Set the TextView as the file name
        txt.setText(device.getName());
      
        // If the item is not a directory, use the file icon

        return row;
	}
	

}
