package bluetooth;

import java.util.Set;

import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.john_aziz57.absence.R;

import data.AbsenceSheet;
import data.Constants;

public class BluetoothSendFragment extends Fragment {
	protected static final int REQUEST_ENABLE_BT = 0;

	private final BluetoothServices btServices;
	private BluetoothArrayAdapter arrayAdapter;
	private ProgressDialog ringProgressDialog;

	private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			// When discovery finds a device
			if (BluetoothDevice.ACTION_FOUND.equals(action)) {
				// Get the BluetoothDevice object from the Intent
				BluetoothDevice device = intent
						.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
				// Add the name and address to an array adapter to show in a
				// ListView
				arrayAdapter.addItem(device);
			}
		}
	};

	private final Handler mHandler = new Handler(Looper.getMainLooper()) {
		public void handleMessage(android.os.Message msg) {

			switch (msg.what) {
			case BluetoothServices.ERROR:
				showErrorToast();
				getActivity().finish();
				break;
			case BluetoothServices.SUCCESS_END:
				btServices.destroy();
				getActivity().finish();
				break;
			case data.Constants.SET_STATE:
				setSendState(msg.arg1);
				break;
			default:
				break;
			}
			// handleMessage(msg);
		}

	};

	/**
	 * 
	 * @param file
	 *            The file to send
	 */
	public BluetoothSendFragment(AbsenceSheet mSheet) {
		btServices = new BluetoothServices(getActivity(), mHandler);
		btServices.setSheetToSend(mSheet);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_bluetooth_devices,
				container, false);
		ListView list = (ListView) rootView.findViewById(R.id.lv_blue_devices);
		arrayAdapter = new BluetoothArrayAdapter(getActivity());
		list.setAdapter(arrayAdapter);
		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				getActivity().unregisterReceiver(mReceiver);// //TODO should be
															// moved to services
				// showRingProgressDialog();
				btServices.connect(arrayAdapter.getItem(arg2));

			}
		});

		if (btServices.hasBluetoothAdapter()) {
			Intent enableBtIntent = new Intent(
					BluetoothAdapter.ACTION_REQUEST_ENABLE);
			startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
		} else {
			// TODO your device doesn't have bluetooth adapter
		}

		return rootView;
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
		case REQUEST_ENABLE_BT:
			if (resultCode == Activity.RESULT_OK) {
				listDevices();
			}else {
				Toast.makeText(this.getActivity(),
						"Failed to start bluetooth service", Toast.LENGTH_LONG)
						.show();
			}
			break;
		}
	}

	private void listDevices() {
		// get the paried devices
		btServices.startDiscovery();
		Set<BluetoothDevice> pairedDevices = btServices.getPairedDevices();
		// If there are paired devices
		if (pairedDevices.size() > 0) {
			// Loop through paired devices
			for (BluetoothDevice device : pairedDevices) {
				// Add the name and address to an array adapter to show in a
				// ListView
				arrayAdapter.addItem(device);
			}
		}

		// Register the BroadcastReceiver
		IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
		getActivity().registerReceiver(mReceiver, filter); // Don't forget to
															// unregister during
															// onDestroy

	}

	@Override
	public void onDestroy() {
		if (btServices != null)
			btServices.destroy();
		super.onDestroy();
	}

	private void showErrorToast() {
		Toast.makeText(getActivity().getApplicationContext(),
				"Error In Connecting ", Toast.LENGTH_SHORT).show();

	}

	// private void showRingProgressDialog(){
	// ringProgressDialog = ProgressDialog.show(getActivity(),
	// "Please wait ...", "", true);
	// ringProgressDialog.setCancelable(false);
	// }

	private void setSendState(int arg2) {
		if (ringProgressDialog == null) {// TODO if i want to send two times it
											// won't be shown for the second
											// time
			ringProgressDialog = ProgressDialog.show(getActivity(),
					"Please wait ...", "", true);
			ringProgressDialog.setCancelable(false);
		}
		switch (arg2) {
		case data.Constants.SEND_STATE_CONNECTING:
			ringProgressDialog.setMessage("Connecting");
			break;
		case data.Constants.SEND_STATE_SENDING:
			ringProgressDialog.setMessage("Sending");
			break;
		}

	}

}