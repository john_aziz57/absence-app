package bluetooth;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Set;
import java.util.UUID;
import data.AbsenceSheet;
import data.Constants;
import data.SheetReader;
import data.SheetWriter;
import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.os.Handler;
import android.os.Message;

public class BluetoothServices {
	// TODO Singleton
	// private static final String MY_UUID =
	// "fa87c0d0-afac-11de-8a39-0800200c9a66";
	private static final String MY_UUID = "00000003-0000-1000-8000-00805F9B34FB";
	static final int ERROR = -1;
	static final int SUCCESS_END = 0;
	private final Context mContext;
	private final Handler mHandler;
	private final BluetoothAdapter blueAdapter;
	private BluetoothDevice blueDevice;
	private ConnectThread mConnectThread;
	private AcceptThread acceptThread;
	private WriteThread mWriteThread;
	private ReadThread mReadThread;

	private AbsenceSheet mSheet;

	public BluetoothServices(Context context, Handler handler) {
		mContext = context;
		mHandler = handler;
		blueAdapter = BluetoothAdapter.getDefaultAdapter();
	}

	public boolean hasBluetoothAdapter() {
		if (blueAdapter == null)
			return false;
		return true;
	}

	/**
	 * Will Cancel Discovery Automatically
	 * 
	 * @param device
	 */
	public synchronized void connect(BluetoothDevice device) {

		blueDevice = (device);
		blueAdapter.cancelDiscovery();
		
		if (mConnectThread != null) {
			mConnectThread.cancel();
			mConnectThread = null;
		}

		// Cancel any thread currently running a connection
		if (mWriteThread != null) {
			mWriteThread.cancel();
			mWriteThread = null;
		}
		
		mConnectThread = new ConnectThread(blueDevice);
		mConnectThread.start();
		setState(Constants.SEND_STATE_CONNECTING);
	}

	private class ConnectThread extends Thread {
		private final BluetoothSocket mBluetoothSocket;
		private final BluetoothDevice mmDevice;

		public ConnectThread(BluetoothDevice device) {
			mmDevice = device;
			BluetoothSocket temp = null;
			try {
				// Method m =
				// device.getClass().getMethod("createInsecureRfcommSocket", new
				// Class[] {int.class});
				// s = (BluetoothSocket) m.invoke(device, 1);
				temp = mmDevice.createRfcommSocketToServiceRecord(UUID
						.fromString(MY_UUID));

			} catch (Exception e) {
				e.printStackTrace();
			}
			mBluetoothSocket = temp;
		}

		@SuppressLint("NewApi")
		@Override
		public void run() {

			try {
				// sleep(1000);
				 if(mBluetoothSocket.isConnected()){
				 mBluetoothSocket.close();
				 }
				mBluetoothSocket.connect();
				
				mWriteThread = new WriteThread(mBluetoothSocket);
				mWriteThread.start();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				 System.out.println(e.toString());
				 System.out.println(e.toString());
				 System.out.println(e.getMessage());
				 System.out.println(e.getMessage());
//				mHandler.obtainMessage(ERROR).sendToTarget();
			}

			// TODO Auto-generated catch block
			// mHandler.obtainMessage(ERROR);

		}

		public void cancel() {
			//
			try {
				mBluetoothSocket.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	private class AcceptThread extends Thread {
		private final BluetoothServerSocket mBluetoothServerSocket;

		public AcceptThread() {
			//
			BluetoothServerSocket temp = null;
			try {
				temp = blueAdapter.listenUsingRfcommWithServiceRecord(
						"BluetoothChatSecure", UUID.fromString(MY_UUID));

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			mBluetoothServerSocket = temp;
		}

		@Override
		public void run() {
			BluetoothSocket socket = null;
			try {
				// TODO

				socket = mBluetoothServerSocket.accept();
				mReadThread = new ReadThread(socket);
				mReadThread.start();
				setState(Constants.RECEIVE_STATE_RECEIVING);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public void cancel() {
			try {
				mBluetoothServerSocket.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	//TODO
	public synchronized void writingStart(){
		
	}

	private class WriteThread extends Thread {
		private final BluetoothSocket mSocket;

		public WriteThread(BluetoothSocket socket) {
			mSocket = socket;
		}

		@Override
		public void run() {

			try {
				OutputStream out = null;

				setState(Constants.SEND_STATE_SENDING);
				out = mSocket.getOutputStream();
				SheetWriter writer = new SheetWriter(out, mSheet);
				writer.writeSheet();
				mHandler.obtainMessage(SUCCESS_END).sendToTarget();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		public void cancel() {
			try {
				mSocket.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	//TODO
	public synchronized void readingStart(){
		
	}
	private class ReadThread extends Thread {
		private InputStream in;

		public ReadThread(BluetoothSocket socket) {
			try {
				in = socket.getInputStream();

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		@Override
		public void run() {
			
			SheetReader reader = new SheetReader(in);
			try {
				AbsenceSheet sh = reader.readSheet();

				Message msg = mHandler.obtainMessage();
				msg.what = SUCCESS_END;
				msg.obj = sh;
				msg.sendToTarget();

			} catch (Exception e) {
				// TODO Auto-generated catch block
				Message msg = mHandler.obtainMessage();
				msg.what = ERROR;
				msg.sendToTarget();
				e.printStackTrace();

			}
		}

		public void cancel() {
			try {
				in.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public boolean startDiscovery() {
		// If we're already discovering, stop it
		if (blueAdapter.isDiscovering()) {
			blueAdapter.cancelDiscovery();
		}
		return blueAdapter.startDiscovery();
	}

	public void destroy() {
		try {
			blueAdapter.cancelDiscovery();
			blueAdapter.disable();

			mConnectThread = null;

			acceptThread = null;

			mWriteThread = null;

			mReadThread = null;

			blueDevice = null;
		} catch (Exception e) {

		}

	}

	public void setSheetToSend(AbsenceSheet sheet) {
		mSheet = sheet;
	}

	public void Accept() {

		acceptThread = new AcceptThread();
		acceptThread.start();
		setState(Constants.RECEIVE_STATE_ACCPETING);
	}

	public Set<BluetoothDevice> getPairedDevices() {
		return blueAdapter.getBondedDevices();
	}

	/**
	 * Sets the state for sending/receiving the dialog box
	 * 
	 * @param state
	 */
	synchronized public void setState(int state) {
		Message msg = mHandler.obtainMessage();
		msg.what = Constants.SET_STATE;
		msg.arg1 = state;
		msg.sendToTarget();
	}
}
