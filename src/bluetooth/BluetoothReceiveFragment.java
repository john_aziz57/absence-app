package bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.john_aziz57.absence.R;

import data.AbsenceSheet;

public class BluetoothReceiveFragment extends Fragment {
	private BluetoothServices btServices;
	private static final int REQUEST_BT_DSCOVERABLE = 0;

	private final Handler mHandler = new Handler(Looper.getMainLooper()) {
		public void handleMessage(android.os.Message msg) {

			switch (msg.what) {
			case BluetoothServices.ERROR:
				showErrorToast();
				break;
			case BluetoothServices.SUCCESS_END:
				btServices.destroy();
				((BluetoothReceiveActivity)getActivity()).returnResult((AbsenceSheet) msg.obj);
				
				
			default:
				break;
			}
			// handleMessage(msg);
		}

	};

	public BluetoothReceiveFragment() {


	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Intent discoverableIntent = new Intent(
				BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
		discoverableIntent.putExtra(
				BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 1000);
		startActivityForResult(discoverableIntent, REQUEST_BT_DSCOVERABLE);

		
		View rootView = inflater.inflate(R.layout.fragment_bluetooth_receive,
				container, false);
		return rootView;
	}

	private void showErrorToast() {
		Toast.makeText(getActivity().getApplicationContext(),
				"Error In Connecting ", Toast.LENGTH_SHORT).show();

	}
	
	@Override
	public void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		if(btServices!=null)
		btServices.destroy();
	}
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
	
		//super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode) {
		case REQUEST_BT_DSCOVERABLE:
			if(resultCode==1000){
				// TODO don't accept till result is returned from this activity
				btServices = new BluetoothServices(getActivity(), mHandler);
				btServices.Accept();
			}
			else{
				Toast.makeText(this.getActivity(),
						"Failed to start bluetooth service", Toast.LENGTH_LONG)
						.show();
				this.getActivity().finish();
			}
			break;

		default:
			break;
		}
	}
}
